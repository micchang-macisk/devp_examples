﻿using Dapper;
using Demo.Csharp.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace Demo.Csharp.Controllers
{
    public class BasicController : ApiController
    {
        [Route("api/basic/class"), HttpGet]
        public IHttpActionResult Watch(int HandLength , int LaceLength) {

            try
            {
                var MyHand = new Hand() { Length = HandLength };
                var Watch = new Watch(LaceLength);
                Watch.Setting(MyHand);
                var MyWatch = (Watch)MyHand.Accessories.FirstOrDefault(c => c is Watch);
                var TimeNow = MyWatch.GetTime();
                return Ok(TimeNow.ToLongTimeString());
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
        
        [Route("api/basic/if"), HttpGet]
        public IHttpActionResult If(int value)
        {
            string Response;
            int Condition = 10;
            if (value > Condition)
            {
                Response = "輸入的值大於10";
            }
            else if (value < 10)
            {
                Response = "輸入的值小於10";
            }
            else
            {
                Response = "輸入的值等於10";
            }

            return Ok(Response);
        }

        [Route("api/basic/switch"), HttpGet]
        public IHttpActionResult Switch(int value)
        {
            string Response;
            switch (value)
            {
                case 1:
                    Response = "數字是" + value;
                    break;             //跳離區段
                case 2:
                    Response = "數字不再條件內";
                    break;
            }
            return Ok();
        }

        [Route("api/basic/for"), HttpGet]
        public IHttpActionResult For(int value)
        {

            int Result = 0;
            for (int i = value; i < 11; i++)
            {
                Result = Result + i;
            }
            return Ok(Result);
        }

        [Route("api/basic/while"), HttpGet]
        public IHttpActionResult While(int value)
        {

            int Result = 0;
            int i = 1;
            while (i < 11)
            {
                Result = Result + i;
                i = i + 1;
            }
            return Ok(Result);
        }

        [Route("api/basic/dowhile"), HttpGet]
        public IHttpActionResult DoWhile(int value)
        {

            int Result = 0;
            int i = 1;
            do
            {
                Result = Result + i;
                i = i + 11;
            }
            while (i < 11);
            return Ok(Result);
        }

        [Route("api/basic/foreach"), HttpGet]
        public IHttpActionResult Foreach()
        {

            int[] Array = new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            foreach (int item in Array)
            {
                Debug.WriteLine(item);

            }
            return Ok();
        }


        [Route("api/basic/exception"), HttpGet]
        public IHttpActionResult Exception()
        {
            try
            {
                //例外處理狀態的攔截區段
            }
            catch (Exception ex)
            {
                //記錄錯誤訊息
                Debug.WriteLine(ex.Message);
                //包裝錯誤訊息後拋出
                throw new Exception(ex.Message);
                //將原有例外物件拋出
                throw;
                //忽略錯誤訊息
            }
            finally
            {
                //無論有無例外都將會執行的區段
                //一般而言會操作資源釋放
            }
            return Ok();
        }


        [Route("api/basic/default_exception"), HttpGet]
        public IHttpActionResult DefaultException()
        {
            try
            {
                new ExceptionService().ThrowDefault();
                return Ok();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [Route("api/basic/status_exception"), HttpGet]
        public IHttpActionResult StatusException()
        {
            try
            {
                new ExceptionService().ThrowStatus();
                return Ok();
            }
            catch (StatusException ex)
            {
                return Content<int>(HttpStatusCode.OK, ex.Status);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [Route("api/basic/tasks"), HttpGet]
        public IHttpActionResult Tasks()
        {
            #region Timer 初始
            var Stopwatch = new Stopwatch();
            Stopwatch.Start();
            #endregion 

            var Tasks = new List<Task<int>>();

            Func<object, int> GetSummaryValue = (Obj) =>
            {
                var Args = (dynamic)Obj;
                Thread.Sleep(1000);
                return Args.Value;
            };

            for (int TIdx = 0; TIdx < 50; TIdx++)
            {

                var Task = new Task<int>(
                    GetSummaryValue,
                    new { Value = TIdx });
                Task.Start();
                Tasks.Add(Task);
            }

            Task.WaitAll(Tasks.ToArray());

            Stopwatch.Stop();

            var Result = new
            {
                Second = Stopwatch.Elapsed.Seconds,
                Summary = Tasks.Sum(s=>s.Result)
            };

            return Ok(Result);
        }

        [Route("api/basic/tasks_compare"), HttpGet]
        public IHttpActionResult TasksCompare()
        {
            var Stopwatch = new Stopwatch();
            Stopwatch.Start();
            var Summary = 0;
            for (int TIdx = 0; TIdx < 50; TIdx++)
            {
                Thread.Sleep(1000);
                Summary += TIdx;
            }
            Stopwatch.Stop();
            var Result = new { Second = Stopwatch.Elapsed.Seconds, Summary = Summary };
            return Ok(Result);
        }

        [Route("api/basic/action"), HttpGet]
        public IHttpActionResult Action()
        {
            Action<dynamic> PlusValue = (Args) =>
            {
                Args.Value = Args.Value + 50;
            };
            using (dynamic Args = new { Value = 50 })
            {
                var Result = PlusValue(Args);
                return Ok(Result);
            }
        }

        [Route("api/basic/func"), HttpGet]
        public IHttpActionResult func()
        {
            Func<int, string> PlusValue = (Value) =>
            {
                Value += 200;
                return (Value + 200).ToString();
            };
            var Result = PlusValue(50);
            return Ok(Result);
        }

        [Route("api/basic/building"), HttpGet]
        public IHttpActionResult GetBuilding()
        {
            return Ok(new Building().GetFurnitures());
        }

        [Route("api/basic/delegate"), HttpGet]
        public IHttpActionResult Delegate(string value)
        {
            new DelegateClass().ShowText(value);
            new DelegateClass().ShowMessage(value);
            return Ok();
        }

        [Route("api/basic/event"), HttpGet]
        public IHttpActionResult Event()
        {

            return Ok();
        }

        [Route("api/basic/attribute"), HttpGet]
        public IHttpActionResult Attribute()
        {
            AttributeClass MyAttribute = new AttributeClass();
            MyAttribute.Begin();
            MyAttribute.Excute();
            return Ok();
        }

        [Route("api/basic/generic"), HttpGet]
        public IHttpActionResult Generic()
        {
            MyClass<int> MyClass = new MyClass<int>();
            string result1 = MyClass.GetType().ToString();
            new Program().Display("MyClass.Data", result1);

            MyClass<bool> MyClass2 = new MyClass<bool>();
            string result2 = MyClass2.GetType().ToString();
            new Program().Display("MyClass.Data", result2);
            return Ok();
        }

        [Route("api/basic/operator"), HttpGet]
        public IHttpActionResult Operator()
        {
            MyOperator MyOperator1 = new MyOperator(1, 2);
            MyOperator MyOperator2 = new MyOperator(3, 5);
            MyOperator MyOperator3 = new MyOperator(4, 6);
            Debug.WriteLine((double)MyOperator1 * MyOperator2 + MyOperator3);
            return Ok();
        }

        [Route("api/basic/query_expression"), HttpGet]
        public IHttpActionResult QueryExpression()
        {
            var List = new ListProgram().CreateList();

            //Query Expression
            IEnumerable<LinqData> People =
                from data in List
                where data.Name == "Amy"
                select data;

            //顯示結果

            foreach (LinqData person in People)
            {
                Debug.WriteLine(person.Name + "是" + person.Age + "歲");
            }
            return Ok();


        }

        [Route("api/basic/method_expression"), HttpGet]
        public IHttpActionResult MethodExpression()
        {
            var List = new ListProgram().CreateList();

            //Method Expression
            var People = List.Where((x) => x.Name == "Amy");

            //包含select的寫法
            //var People = list.Where((x) => x.Name == "Amy").Select((x)=>x);

            //顯示結果

            foreach (LinqData person in People)
            {
                Debug.WriteLine(person.Name + "是" + person.Age + "歲");
            }
            return Ok();


        }

        [Route("api/basic/first"), HttpGet]
        public IHttpActionResult First()
        {
            var List = new ListProgram().CreateList();

            // 這個 Person1 是單個物件，也就是 MyData Person1
            var Person1 = List.FirstOrDefault((x) => x.Age < 22);
            Debug.WriteLine("小於22歲第一個被找到的人是:" + Person1.Name);

            //因為找不到 所以會跳出例外
            var Person2 = List.First((x) => x.Age < 20);
            Debug.WriteLine("小於20歲第一個被找到的人是:" + Person2.Name);

            return Ok();


        }

        [Route("api/basic/last"), HttpGet]
        public IHttpActionResult Last()
        {
            var List = new ListProgram().CreateList();

            // 這個 Person1 是單個物件，也就是 MyData Person1
            var Person1 = List.LastOrDefault((x) => x.Age > 20);
            Debug.WriteLine("大於20歲最後一個被找到的人是:" + Person1.Name);

            //因為找不到 所以會跳出例外
            var Person2 = List.Last((x) => x.Age > 50);
            Debug.WriteLine("大於50歲最後一個被找到的人是:" + Person2.Name);

            return Ok();


        }

        [Route("api/basic/single"), HttpGet]
        public IHttpActionResult Single()
        {
            var List = new ListProgram().CreateList();

            // 這個 Person1 是單個物件，也就是 MyData Person1
            var Person1 = List.SingleOrDefault((x) => x.Name == "Ben");
            Debug.WriteLine("找到唯一的人是:" + Person1.Name + "-" + Person1.Age + "歲");

            //因為找不到唯一，因為有兩個Gary，所以會跳出例外
            var Person2 = List.Single((x) => x.Name == "Gary");
            Debug.WriteLine("找到唯一的人是:" + Person2.Name + "-" + Person2.Age + "歲");

            return Ok();


        }

        [Route("api/basic/any"), HttpGet]
        public IHttpActionResult Any()
        {
            var List = new ListProgram().CreateList();

            string Name = "Jhon";
            var Result = List.Any((x) => x.Name == "Jhon");
            if (Result)
            {
                Debug.WriteLine("找到:" + Name);
            }
            else
            {
                Debug.WriteLine("找不到:" + Name);
            }

            return Ok();


        }

        [Route("api/basic/all"), HttpGet]
        public IHttpActionResult All()
        {
            var List = new ListProgram().AllList();


            var IsAllGary = List.All((x) => x.Name == "Gary");
            if (IsAllGary)
            {
                Debug.WriteLine("全部的人都叫Gary");
            }
            else
            {
                Debug.WriteLine("有些人不叫Gary");
            }

            var IsAllOverThirty = List.All((x) => x.Age >= 30);
            if (IsAllOverThirty)
            {
                Debug.WriteLine("大家都超過30歲");
            }
            else
            {
                Debug.WriteLine("有人不到30歲");
            }
            return Ok();


        }

        [Route("api/basic/groupby"), HttpGet]
        public IHttpActionResult GroupBy()
        {
            var List = new ListProgram().CreateList();
            var Result = List.GroupBy((x) => x.City);
            foreach (var item in Result)
            {
                Debug.WriteLine("住在:" + item.Key);
                foreach (var Person in item)
                {
                    Debug.WriteLine(Person.Name);
                }

                Debug.WriteLine("----------------------");
            }

            return Ok();



        }

        [Route("api/basic/query_groupby"), HttpGet]
        public IHttpActionResult QueryGroupBy()
        {
            var List = new ListProgram().CreateList();
            var Result =
                from o in List
                group o by o.City;

            foreach (var item in Result)
            {
                Debug.WriteLine("住在:", item.Key);

                foreach (var Person in item)
                {
                    Debug.WriteLine(Person.Name);
                }
                Debug.WriteLine("-------------------");
            }

            return Ok();
        }

        [Route("api/basic/join"), HttpGet]
        public IHttpActionResult Join()
        {
            var Teachers = new ListProgram().CreateTeachers();
            var Students = new ListProgram().CreateStudents();
            var Result =
                from teacher in Teachers
                join student in Students
                on teacher.ClassName equals student.ClassName
                select new ResultInFo
                { ClassName = teacher.ClassName, Teacher = teacher.Teacher, Student = student.Student };

            foreach (var item in Result)
            {
                Debug.WriteLine(item.ClassName + "班," + "老師:" + item.Teacher + ",學生:" + item.Student);
            }


            return Ok();
        }

        [Route("api/basic/orderby"), HttpGet]
        public IHttpActionResult OrderBy()
        {
            var List = new ListProgram().CreateList();
            //依年紀由小到大排序
            var Order1 = List.OrderBy((x) => x.Age);
            new ListProgram().Display(Order1);
            //依年紀由大到小排序
            var Order2 = List.OrderByDescending((x) => x.Age);
            new ListProgram().Display(Order2);
            //依名字由小到大排序，再依照年紀由小到大排序
            var Order3 = List.OrderBy((x) => x.Name).ThenBy((x) => x.Age);
            new ListProgram().Display(Order3);
            //依名字由大到小排序，再依照年紀由大到小排序
            var Order4 = List.OrderBy((x) => x.Name).ThenByDescending((x) => x.Age);
            new ListProgram().Display(Order4);

            return Ok();


        }

        [Route("api/basic/query_orderby"), HttpGet]
        public IHttpActionResult QueryOrderBy()
        {
            var List = new ListProgram().CreateList();
            var Order1 =
                from o in List
                orderby o.Name, o.Age
                select o;
            new ListProgram().Display(Order1);

            var Order2 =
                from o in List
                orderby o.Name descending, o.Age descending
                select o;
            new ListProgram().Display(Order2);

            return Ok();

        }

        [Route("api/basic/linq_operation"), HttpGet]
        public IHttpActionResult LinqOperation()
        {
            var List = new ListProgram().CreateList();
            //計算List中，所有年齡的總和
            int Total = List.Sum((x) => x.Age);
            Debug.WriteLine("年齡總和為:" + Total);
            //取得List中，年齡的最小值
            int MinAge = List.Min((x) => x.Age);
            Debug.WriteLine("年齡最小值為:" + MinAge);
            //取得List中，年齡的最大值
            int MaxAge = List.Max((x) => x.Age);
            Debug.WriteLine("年齡最大值為:" + MaxAge);
            //取得List中的數量
            int Count = List.Count();
            Debug.WriteLine("List的總數為:" + Count);
            int CountOfGary = List.Count((x) => x.Name == "Gary");
            Debug.WriteLine("List中Gary的總數量為:" + CountOfGary);
            //取得所有年齡的平均值
            var Average = List.Average((x) => x.Age);
            Debug.WriteLine("年齡平均值為:" + Average);

            return Ok();
        }

        [Route("api/basic/distinct"), HttpGet]
        public IHttpActionResult Distinct()
        {
            var List = new List<string> { "台北", "東京", "台北", "台北", "紐約", "東京" };
            var Result = List.Distinct();
            foreach (var item in Result)
            {
                Debug.WriteLine(item);
            }
            return Ok();
        }

        [Route("api/basic/skip_take"), HttpGet]
        public IHttpActionResult SkipTake()
        {
            var List = new List<string> { "A", "B", "C", "D", "E", "F", "G", "G" };
            var ResultOfSkip = List.Skip(3);
            Debug.WriteLine("Skip(3)的結果");
            new ListProgram().GetResult(ResultOfSkip);

            var ResultOfTake = List.Take(3);
            Debug.WriteLine("Take(3)的結果");
            new ListProgram().GetResult(ResultOfTake);

            var ResultOfSkipTake = List.Skip(4).Take(3);
            Debug.WriteLine("Skip(4).Take(3)的結果");
            new ListProgram().GetResult(ResultOfSkipTake);
            return Ok();
        }

        [Route("api/basic/elementat"), HttpGet]
        public IHttpActionResult ElementAt()
        {
            int Index = 2;
            var List = new ListProgram().List();
            var Person = List.ElementAtOrDefault(Index);
            if (Person != null)
            {
                Debug.WriteLine("找到索引為 : " + Index.ToString() + " 的人是" + Person.Name + " - " + Person.Age + "歲");
            }
            else
            {
                Debug.WriteLine("查無此人");
            }
            return Ok();
        }

        [Route("api/basic/adddept"), HttpPost]
        public IHttpActionResult AddDept()
        {
            var Conn = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\huwyang\Documents\Projects\macisk\devp_examples\demo\Demo.Csharp\App_Data\LinqDB.mdf;Integrated Security=True;MultipleActiveResultSets=True;Application Name=EntityFramework");
            string Query = "INSERT INTO Dept(No,Id,Name)  VALUES(@no,@id,@name)";
            Conn.Execute(Query, new {no="1",id="micchang", name = "張閔傑" });

            return Ok();
        }


    }
}




