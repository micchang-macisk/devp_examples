﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Demo.Csharp.Models
{
    /// <summary>
    /// 大樓
    /// </summary>
    public class Building
    {
        /// <summary>
        /// 建構式
        /// 新增大樓
        /// </summary>
        public Building()
        {
           
            Floors = new List<IFloor>();
            Floors.Add(new FirstFloor());
            Floors.Add(new SecondFloor());
        }

        /// <summary>
        /// 實作介面
        /// 樓層
        /// </summary>
        List<IFloor> Floors { get; set; }

        /// <summary>
        /// 取得大樓裡的所有家具的位置清單 
        /// </summary>
        /// <returns> 位置清單</returns>
        public IEnumerable<string> GetFurnitures()
        {
            List<string> Positions = new List<string>();    //家具位置清單
            foreach (var Floor in Floors)                  //大樓裡的樓層
            {
                foreach (var Partition in Floor.Partitions) //樓層裡的隔間
                {
                    foreach (var Furniture in Partition.Furnitures)  //隔間裡的家具
                    {
                        Positions.Add($"{Floor.FloorNumber}的{Partition.Type}隔間有一個{Furniture.Store}的{Furniture.Type}");
                        //yield return $"{Floor.FloorNumber}的{Partition.Type}隔間有一個{Furniture.Store}的{Furniture.Type}";
                    }

                }


            }
            return Positions;
        }
    }
}