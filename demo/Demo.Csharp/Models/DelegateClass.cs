﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace Demo.Csharp.Models
{
    public delegate void Actions(string message = null);
    public class DelegateClass
    {
        

    public  void ShowText(string msg)
        {
            Debug.WriteLine($"ShowText{msg}");
        }

    public  void ShowMessage(string msg)
        {
            Debug.WriteLine($"ShowMessage{msg}");
            
        }
    }
}