﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Demo.Csharp.Models
{
    public class ExceptionService
    {
        public ExceptionService() {
        }

        public void ThrowDefault() {
            throw new Exception("can nat initial!");
        }

        public void ThrowStatus() { throw new StatusException(50); }
    }

    public class StatusException:Exception
    {
        public int Status { get; set; }
        public StatusException(int Status) {
            this.Status = Status;
        }
    }
}