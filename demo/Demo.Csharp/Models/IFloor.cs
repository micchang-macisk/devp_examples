﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Demo.Csharp.Models
{
    /// <summary>
    /// 每層樓的隔間
    /// 第幾層樓
    /// </summary>
    public interface IFloor
    {
        List<IPartition> Partitions { get; set; }
        string FloorNumber { get; set; }

    }

    /// <summary>
    /// 擴充方法
    /// 把家具放入隔間裡，並給商店名稱
    /// </summary>
    public static class IFloorExtension   
    {
        public  static void SetPartitions(this IFloor FloorThis,string Store)
        {
            var Kitchen = new Kitchen();
            Kitchen.Furnitures.Add(new Stove(Store));
            FloorThis.Partitions.Add(Kitchen);
            var LivingRoom = new LivingRoom();
            LivingRoom.Furnitures.Add(new Sofa(Store));
            FloorThis.Partitions.Add(LivingRoom);
            var BedRoom = new BedRoom();
            BedRoom.Furnitures.Add(new Bed(Store));
            FloorThis.Partitions.Add(BedRoom);
            var BathRoom = new BathRoom();
            BathRoom.Furnitures.Add(new Bathtub(Store));
            FloorThis.Partitions.Add(BathRoom);
        }
    }

    public class FirstFloor : IFloor
    {
        /// <summary>
        /// 建構式
        /// 設定樓層和家具商店名稱
        /// </summary>
        public  FirstFloor()   
        {
            Partitions = new List<IPartition>();
            this.SetPartitions("宜家");
            FloorNumber = "第一層樓";
        }
        /// <summary>
        ///  實作介面
        /// 隔間與第幾樓層
        /// </summary>
        public List<IPartition> Partitions {get;set;}
        public string FloorNumber { get; set; }
    }

    public class SecondFloor : IFloor
    {
        /// <summary>
        /// 建構式設
        /// 定樓層和家具商店名稱
        /// </summary>
        public SecondFloor()
        {
            Partitions = new List<IPartition>();
            this.SetPartitions("大潤發");
            FloorNumber = "第二層樓";
        }

        /// <summary>
        /// 實作介面
        /// 隔間與第幾樓層
        /// </summary>
        public List<IPartition> Partitions { get; set; }
        public string FloorNumber { get; set; }

    }

}