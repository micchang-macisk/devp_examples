﻿namespace Demo.Csharp.Models
{
    /// <summary>
    /// 家具商店名稱和家具類型
    /// </summary>
    public interface IFurniture
    {
         string Store { get; set; }

         string Type { get; set; }

    }
    public class Bed : IFurniture
    {
        /// <summary>
        /// 建構式
        /// 設定家具類型名稱和家具商店名稱
        /// </summary>
        /// <param name="Store">家具商店名稱</param>
        public Bed(string Store)
        {
            Type = "床";
            this.Store = Store;
        }

        /// <summary>
        /// 實作介面
        /// 家具商店名稱和家具類型
        /// </summary>
        public string Store { get ; set ; }
        public string Type { get ; set; }
    }


    public class Sofa : IFurniture
    {
        /// <summary>
        /// 建構式
        /// 設定家具類型名稱和家具商店名稱
        /// </summary>
        /// <param name="Store">家具商店名稱</param>
        public Sofa(string Store)
        {
            Type = "沙發";
            this.Store = Store;
        }

        /// <summary>
        /// 實作介面
        /// 家具商店名稱和家具類型
        /// </summary>
        public string Store { get; set; }
        public string Type { get; set; }
    }


    public class Stove : IFurniture
    {
        /// <summary>
        /// 建構式
        /// 設定家具類型名稱和家具商店名稱
        /// </summary>
        /// <param name="Store">家具商店名稱</param>
        public Stove(string Store)
        {
            Type = "爐具";
            this.Store = Store;
        }

        /// <summary>
        /// 實作介面
        /// 家具商店名稱和家具類型
        /// </summary>
        public string Store { get; set; }
        public string Type { get; set; }
    }


    public class Bathtub : IFurniture
    {
        /// <summary>
        /// 建構式
        /// 設定家具類型名稱和家具商店名稱
        /// </summary>
        /// <param name="Store">家具商店名稱</param>
        public Bathtub(string Store)
        {
            Type = "浴缸";
            this.Store = Store;
        }

        /// <summary>
        /// 實作介面
        /// 家具商店名稱和家具類型
        /// </summary>
        public string Store { get; set; }
        public string Type { get; set; }
    }
}