﻿using System.Collections.Generic;

namespace Demo.Csharp.Models
{
    /// <summary>
    /// 隔間和隔間種類
    /// </summary>
    public interface IPartition   
    {
        List<IFurniture> Furnitures { get; set; }
        string Type { get; set; }
    }

    public class Kitchen : IPartition
    {
        /// <summary>
        /// 建構式
        /// 設定隔間種類
        /// </summary>
        public Kitchen()
        {
            Furnitures = new List<IFurniture>();
            Type = "廚房";
        }

        /// <summary>
        /// 實作介面
        /// 隔間與隔間種類
        /// </summary>
        public List<IFurniture> Furnitures { get; set; }
        public string Type { get ; set; }
    }

    public class LivingRoom : IPartition
    {
        /// <summary>
        /// 建構式
        /// 設定隔間種類
        /// </summary>
        public LivingRoom()
        {
            Furnitures = new List<IFurniture>();
            Type = "客廳";
        }

        /// <summary>
        /// 實作介面
        /// 隔間與隔間種類
        /// </summary>
        public List<IFurniture> Furnitures { get; set; }
        public string Type { get; set; }
    }

    public class BathRoom : IPartition
    {
        /// <summary>
        /// 建構式
        /// 設定隔間種類
        /// </summary>
        public BathRoom()
        {
            Furnitures = new List<IFurniture>();
            Type = "浴室";
        }

        /// <summary>
        /// 實作介面
        /// 隔間與隔間種類
        /// </summary>
        public List<IFurniture> Furnitures { get; set; }
        public string Type { get; set; }
    }

    public class BedRoom : IPartition
    {
        /// <summary>
        /// 建構式
        /// 設定隔間種類
        /// </summary>
        public BedRoom()
        {
            Furnitures = new List<IFurniture>();
            Type = "臥室";
        }

        /// <summary>
        /// 實作介面
        /// 隔間與隔間種類
        /// </summary>
        public List<IFurniture> Furnitures { get; set; }
        public string Type { get; set; }
    }

}