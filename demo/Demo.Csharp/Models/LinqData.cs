﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace Demo.Csharp.Models
{
    public class LinqData
    {
        public string Name { get; set; }

        public int Age { get; set; }

        public string City { get; set; }
    }

    public class TeacherInFo
    {
        public string ClassName { get; set; }
        public string Teacher { get; set; }
    }

    public class StudentInFo
    {
        public string ClassName { get; set; }
        public string Student { get; set; }
    }

    public class ResultInFo
    {
        public string ClassName { get; set; }
        public string Teacher { get; set; }
        public string Student { get; set; }
    }

    public class ListProgram
    {
        public  List<LinqData> CreateList()
        {
           List<LinqData> list = new List<LinqData>();
            list.Add(new LinqData() { Name = "Amy" , Age = 20, City = "台北" });
            list.Add(new LinqData() { Name = "Gary", Age = 41, City = "台北" });
            list.Add(new LinqData() { Name = "Ben" , Age = 27, City = "高雄" });
            list.Add(new LinqData() { Name = "Tom" , Age = 36, City = "台中" });
            list.Add(new LinqData() { Name = "Jhon", Age = 45, City = "台中" });
            list.Add(new LinqData() { Name = "Gary", Age = 22, City = "台北" });
            return list;
        }

        public List<LinqData> AllList()
        {
            List<LinqData> list = new List<LinqData>();
            list.Add(new LinqData() { Name = "Gary", Age = 47 });
            list.Add(new LinqData() { Name = "Gary", Age = 55 });
            list.Add(new LinqData() { Name = "Gary", Age = 27 });
            list.Add(new LinqData() { Name = "Gary", Age = 36 });
            list.Add(new LinqData() { Name = "Gary", Age = 45 });
            list.Add(new LinqData() { Name = "Gary", Age = 32 });
            return list;
        }

        public List<LinqData>List()
        {
            var List = new List<LinqData>()
            {
                new LinqData() { Name = "Amy" , Age = 20, City = "台北" },
                new LinqData() { Name = "Gary", Age = 41, City = "台北" },
                new LinqData() { Name = "Ben", Age = 27, City = "高雄" },
                new LinqData() { Name = "Tom", Age = 36, City = "台中" },
                new LinqData() { Name = "Jhon", Age = 45, City = "台中" }
        };
            return List;
        }

        public List<TeacherInFo>CreateTeachers()
        {
            var list = new List<TeacherInFo>();
            list.Add(new TeacherInFo() {ClassName = "甲",Teacher = "Amy"  });
            list.Add(new TeacherInFo() {ClassName = "乙",Teacher = "Gary" });
            return list;
        }

        public List<StudentInFo>CreateStudents()
        {
            var list = new List<StudentInFo>()
            {
                new StudentInFo(){ClassName="甲",Student="兔兔"},
                new StudentInFo(){ClassName="甲",Student="熊大"},
                new StudentInFo(){ClassName="乙",Student="唐老鴨"},
                new StudentInFo(){ClassName="乙",Student="米妮"},
                new StudentInFo(){ClassName="甲",Student="莎莉"},
                new StudentInFo(){ClassName="乙",Student="高飛"},
                new StudentInFo(){ClassName="乙",Student="布魯托"}
            };
            return list;
        }


        public void Display(IOrderedEnumerable<LinqData>Source)
        {
            foreach(var item in Source)
            {
                Debug.WriteLine("名字 : "+item.Name +"  年齡 : " + item.Age);
            }
            Debug.WriteLine("-------------");
        }

        public void GetResult(IEnumerable<string>Source)
        {
            foreach(var item in Source)
            {
                Debug.WriteLine(item);
            }
        }
    }
}