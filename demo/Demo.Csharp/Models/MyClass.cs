﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace Demo.Csharp.Models
{
    public class MyClass<T>
    {
        public T Data { get; set; }
    }
    public class  Program
    {
        public void Display(string varName, string typeString)
        {
           
            Debug.WriteLine($"{varName}的型別是{typeString}");
        }
    }

}