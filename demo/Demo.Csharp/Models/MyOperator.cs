﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Demo.Csharp.Models
{
    public class MyOperator
    {
        int Num, Den;

        public MyOperator (int Num, int Den)
        {
            this.Num = Num;
            this.Den = Den;
        }

        /// <summary>
        /// 多載 operator +
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static MyOperator operator + (MyOperator a, MyOperator b)
        {
            return new MyOperator(a.Num * b.Den + a.Den * b.Num,a.Den*b.Den);
        }

        /// <summary>
        /// 多載 operator +
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>       
        public static  MyOperator operator * (MyOperator a,MyOperator b)
        {
            return new MyOperator(a.Num * b.Den,a.Den*b.Den);
        }

        /// <summary>
        /// 隱含轉換  MyOperator類型 轉換成 double 類型的運算子
        /// </summary>
        /// <param name="f"></param>
        public static implicit operator double(MyOperator f)
        {
            return (double)f.Num / f.Den;
        }
     }
}