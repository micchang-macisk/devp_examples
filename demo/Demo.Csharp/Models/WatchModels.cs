﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Demo.Csharp.Models
{
    /// <summary>
    /// 時間模組的抽象類別
    /// </summary>
    public abstract class TimerModule
    {
        /// <summary>
        /// 取得時間的抽象方法
        /// </summary>
        /// <returns></returns>
        public abstract DateTime GetTime();

        /// <summary>
        /// 時間模組的安裝電池方式
        /// </summary>
        /// <param name="Battery"></param>
        public virtual void SetBattery(Battery Battery)
        {

        }
    }

    /// <summary>
    /// 手錶
    /// 繼承 TimerModule 時間模組的抽象介面
    /// 實作 IAccessories 綁帶的介面
    /// </summary>
    public class Watch : TimerModule, IAccessories
    {
        public Watch(int Length) {
            this.Lace = new Lace() { Length = Length };
        }

        // 覆寫 TimerModule 抽象類別的抽象方法
        public override DateTime GetTime()
        {
            return DateTime.Now;
        }
        // 覆寫 TimerModule 類別的虛擬方法
        public override void SetBattery(Battery Battery)
        {

        }

        //實作 IAccessories 介面的掛飾的綁帶物件
        public Lace Lace { get; set; }
        //Lace IAccessories.Lace { get; set; }

        //實作 IAccessories 介面的安裝綁帶方法
        public void Setting(Hand Hand)
        {
            if (Lace.Length < Hand.Length) throw new Exception("手太粗囉!");
            Hand.Accessories.Add(this);
        }

    }

    /// <summary>
    /// 掛飾介面
    /// </summary>
    public interface IAccessories
    {
        /// <summary>
        /// 綁帶
        /// </summary>
        Lace Lace { get; set; }


        void Setting(Hand Hand);
    }

    /// <summary>
    /// 手的類別
    /// </summary>
    public class Hand {
        public int Length { get; set; }
        public List<IAccessories> Accessories = new List<IAccessories>();
    }

    /// <summary>
    /// 綁帶類別
    /// </summary>
    public class Lace
    {
        public int Length { get; set; }
        public Lace() {

        }

        
    }

    /// <summary>
    /// 電池類別
    /// </summary>
    public class Battery
    {

    }
}