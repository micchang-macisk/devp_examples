﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Demo.FileHelper
{
    public class Condition
    {
        public Condition() { }
        public Condition(string Query="",string DirectoryId="")
        {
            this.Query = (!string.IsNullOrEmpty(Query)) ? Query:string.Empty;
            this.DirectoryId = (!string.IsNullOrEmpty(DirectoryId)) ? DirectoryId : string.Empty;
        }
        public string DirectoryId { get; set; }
        public string Keyword { get; set; }
        public string Query { get; set; }
        public bool? IsWithDetail { get; set; }
        public bool? IsShared { get; set; }
        public bool? IsDirectory { get; set; }
        public bool? IsWithPermission { get; set; }
        public IEnumerable<string> FileIds { get; set; }
    }
}