﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.FileHelper.Extensions
{
    public static class FileExplorerExtensions
    {
        public static IEnumerable<T> ToList<T>(this IFile FileThis) where T:IFile
        {
            return new List<T>() { (T)FileThis };
        }
        public static bool IsAny<T>(this IEnumerable<T> data)
        {
            return data != null && data.Any();
        }
    }
}
