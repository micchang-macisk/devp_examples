﻿using Demo.FileHelper.Extensions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.FileHelper
{
    public class FileExplorer
    {        

        public FileExplorer(FileProxy FileProxy)
        {
            this.Proxy = (FileProxy != null) ? FileProxy : throw new Exception("Should Assign FileProxy");
        }

        private FileProxy Proxy
        {
            get;set;
        }

        public IFile Directory
        {
            get; set;
        }

        public IEnumerable<IFile> ChangeToDirectory(IFile Directory)
        {
            this.Directory = Directory;
            return new List<IFile>() { Directory };
            //return List(new Condition(DirectoryId:Directory.Id));
        }

        public IEnumerable<IFile> ChangeToDirectory(string DirectoryId)
        {
            if (!string.IsNullOrEmpty(DirectoryId))
                this.Directory = Proxy.GetDirectory(DirectoryId);
            return new List<IFile>() { Directory };
            //return List(new Condition(DirectoryId: Directory.Id));
        }

        public IEnumerable<OnlineFile> List(Condition Condition)
        {
            var ListTask = Proxy.ListAsync(Condition);
            ListTask.Wait();
            return ListTask.Result;
        }

        #region Get

        public IEnumerable<OnlineFile> Get(IEnumerable<string> FileIds)
        {
            var Files = new List<Task<OnlineFile>>();
            foreach (var FileId in FileIds)
                Files.Add(Proxy.GetAsync(FileId));
            Task.WaitAll(Files.ToArray());
            var Result = Files.Select(s => s.Result).ToList();
            return Result;
        }

        public IEnumerable<OnlineFile> Get(string FileIds)
        {
            return Get(FileIds.Split(new string[] { ";" ,"," } , StringSplitOptions.RemoveEmptyEntries));
        }

        #endregion

        #region Move 

        public IEnumerable<OnlineFile> Move(IEnumerable<string> FileIds,string FolderId) {
            var Files = new List<Task<OnlineFile>>();
            foreach (var FileId in FileIds)
                Files.Add(Proxy.MoveAsync(FileId,FolderId));
            Task.WaitAll(Files.ToArray());
            var Result = Files.Select(s => s.Result).ToList();
            return Result;
        }

        public IEnumerable<OnlineFile> Move(string FileIds, string FolderId)
        {
            return Move(FileIds.Split(new string[] { ";", "," }, StringSplitOptions.RemoveEmptyEntries),FolderId);
        }

        #endregion
        
        #region Update

        public IEnumerable<OnlineFile> Update(IEnumerable<OnlineFile> Files)
        {
            var UpdateTasks = new List<Task<OnlineFile>>();
            foreach (var File in Files)
                UpdateTasks.Add(Proxy.UpdateAsync(File));
            Task.WaitAll(UpdateTasks.ToArray());
            foreach (var Task in UpdateTasks)
                yield return Task.Result; 
        }

        public IEnumerable<OnlineFile> Update(OnlineFile File)
        {
            return Update(File.ToList<OnlineFile>());
        }

        #endregion

        #region Create
        public IEnumerable<CreateFile> Create(IEnumerable<CreateFile> Files)
        {
            var CreateTasks = new List<Task<CreateFile>>();
            foreach (var File in Files)
                CreateTasks.Add(Proxy.CreateAsync(File, Directory.ToList<IFile>()));
            Task.WaitAll(CreateTasks.ToArray());
            var Result = new List<CreateFile>();
            foreach (var Task in CreateTasks)
               Result.Add( Task.Result);
            return Result;
        }

        public IEnumerable<CreateFile> Create(CreateFile File)
        {
            yield return File;
        }
        #endregion


        #region Folder
        public IEnumerable<CreateDirectory> Folder(IEnumerable<CreateDirectory> Files)
        {
            var CreateTasks = new List<Task<CreateDirectory>>();
            foreach (var File in Files)
                CreateTasks.Add(Proxy.FolderAsync(File, Directory.ToList<IFile>()));
            Task.WaitAll(CreateTasks.ToArray());
            var Result = CreateTasks.Select(s=>s.Result).ToList();
            return Result;
        }

        public IEnumerable<CreateDirectory> Folder(string FolderNames)
        {
            return Folder(
                FolderNames.Split(new string[] { ",",";"},StringSplitOptions.RemoveEmptyEntries).Select(s=>new CreateDirectory() { Name=s ,MimeType= "application/vnd.google-apps.folder", IsDirectory =true })
            );
        }
        #endregion

        #region Delete

        public IEnumerable<IFile> Delete(IEnumerable<string> FileIds)
        {
            var DeleteTasks = new List<Task<IFile>>();
            foreach (var FileId in FileIds)
                DeleteTasks.Add(Proxy.DeleteAsync(FileId));
            Task.WaitAll(DeleteTasks.ToArray());
            var Result = DeleteTasks.Select(s => s.Result).ToList();
            return Result;
        }



        public IEnumerable<IFile> Delete(string FileIds)
        {
            return Delete(FileIds.Split(new string[] { ";", "," }, StringSplitOptions.RemoveEmptyEntries));
        }
        #endregion

        #region Content
                
        public IEnumerable<ContentFile> Content(IEnumerable<string> FileIds)
        {
            var ContentTasks = new List<Task<ContentFile>>();
            foreach (var FileId in FileIds)
                ContentTasks.Add(Proxy.ContentAsync(FileId));
            Task.WaitAll(ContentTasks.ToArray());
            var Result = ContentTasks.Select(s => s.Result).ToList();
            return Result;
        }

        
        public IEnumerable<ContentFile> Content(string FileIds)
        {
            return Content(FileIds.Split(new string[] { ";", "," }, StringSplitOptions.RemoveEmptyEntries));
        }

        #endregion
        
        #region Permission

        public IEnumerable<IPermission> CreatePermission(IEnumerable<string> Accounts , IEnumerable<string> FileIds , PermissionRoles Role= PermissionRoles.reader , PermissionTypes Type = PermissionTypes.user)
        {
            var Permissions = new List<IPermission>();
            foreach (var FileId in FileIds)
                foreach (var Account in Accounts)
                {
                    var Permission = new Permission()
                    {
                        FileId = FileId,
                        GrantTo = Account,
                        Role = Role,
                        State = false,
                        Type = Type
                    };
                    Permissions.Add(Permission);
                }
            var PermissionTasks = Proxy.CreatePermissionAsync(Permissions);
            Task.WaitAll(PermissionTasks.ToArray());
            var Result = PermissionTasks.Select(s=>s.Result).ToList();
            return Result;
        }

        public IEnumerable<IPermission> CreateUserPermission(string Accounts ,string FileIds,PermissionRoles Roles)
        {
            return CreatePermission(
                    Accounts.Split(new string[] { ";", "," }, StringSplitOptions.RemoveEmptyEntries) ,
                    FileIds.Split(new string[] { ";", "," }, StringSplitOptions.RemoveEmptyEntries) ,
                    Roles ,
                    PermissionTypes.user                    
                );
        }
        public IEnumerable<IPermission> CreateDomainPermission(string Domains, string FileIds, PermissionRoles Roles)
        {
            return CreatePermission(
                    Domains.Split(new string[] { ";", "," }, StringSplitOptions.RemoveEmptyEntries),
                    FileIds.Split(new string[] { ";", "," }, StringSplitOptions.RemoveEmptyEntries),
                    Roles,
                    PermissionTypes.domain
                );
        }



        public IEnumerable<IPermission> GetPermission(IEnumerable<string> FileIds)
        {
            var Files = new List<Task<IEnumerable<IPermission>>>();
            foreach (var FileId in FileIds)
                Files.Add(Proxy.GetPermissionAsync(FileId));
            Task.WaitAll(Files.ToArray());
            var Result = Files.SelectMany(s => s.Result).ToList();
            return Result;
        }

        public IEnumerable<IPermission> GetPermission(string FileIds)
        {
            return GetPermission(FileIds.Split(new string[] { ";", "," }, StringSplitOptions.RemoveEmptyEntries));
        }


        public IEnumerable<IPermission> SyncPermission(IEnumerable<string> TargetIds, IEnumerable<IPermission> Permissions)
        {
            var Tasks = new List<Task<IEnumerable<IPermission>>>();
            foreach (var FileId in TargetIds)
                Tasks.Add(Proxy.SyncPermission(Permissions, FileId));
            Task.WaitAll(Tasks.ToArray());
            return GetPermission(TargetIds);
        }


        public IEnumerable<IPermission> SyncPermission(IEnumerable<string> TargetIds, IEnumerable<string> SourceIds)
        {
            var Permissions = GetPermission(SourceIds);
            return SyncPermission(TargetIds, Permissions);
        }

        public IEnumerable<IPermission> SyncPermission(string TargetIds,string SourceIds)
        {
            return SyncPermission(
                TargetIds.Split(new string[] { ",",";" }, StringSplitOptions.RemoveEmptyEntries) ,
                SourceIds.Split(new string[] { ",", ";" }, StringSplitOptions.RemoveEmptyEntries)
                );
        }

        #endregion

    }
}