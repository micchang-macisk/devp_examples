﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.FileHelper
{
    public abstract class FileProxy
    {
        public abstract void InitailCredential();

        public abstract IFile GetDefaultDirectory();
        public abstract IFile GetDirectory(string FileId);


        public abstract Task<CreateFile> CreateAsync(CreateFile File, IEnumerable<IFile> Directories);
        public abstract Task<CreateDirectory> FolderAsync(CreateDirectory File, IEnumerable<IFile> Directories);

        public abstract Task<IFile> DeleteAsync(string FileId);

        public abstract Task<OnlineFile> UpdateAsync(OnlineFile File);

        public abstract Task<IEnumerable<OnlineFile>> ListAsync(Condition Condition);

        public abstract Task<OnlineFile> GetAsync(string FileId);

        public abstract Task<ContentFile> ContentAsync(string FileId);


        public abstract Task<OnlineFile> MoveAsync(string FileId,string FolderId);

        public FileExplorer GetExplorer() {
            var Explorer = new FileExplorer(this);
            Explorer.ChangeToDirectory(GetDefaultDirectory());
            return Explorer;
        }

        public abstract List<Task<IPermission>> CreatePermissionAsync(IEnumerable<IPermission> Permissions);

        public abstract Task<IEnumerable<IPermission>> GetPermissionAsync(string FileId);

        public abstract Task<IEnumerable<IPermission>> SyncPermission(IEnumerable<IPermission> permissions, string FileId);
    }
}