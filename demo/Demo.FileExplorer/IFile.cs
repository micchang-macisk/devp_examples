﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Demo.FileHelper
{
    public interface IFile
    {
        string Name { get; set; }
        string Id { get; set; }
        string MimeType { get; set; }
        bool IsDirectory { get; set; }
    }
}