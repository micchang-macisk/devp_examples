﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Demo.FileHelper
{
    public interface IFileDetail
    {
        IEnumerable<IFile> Parents { get; set; }
        Dictionary<string, string> Properties { get; set; }
        string Owner { get; set; }
        string Extension { get; set; }
    }
}