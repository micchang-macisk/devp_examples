﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.FileHelper
{

    public enum PermissionTypes
    {
        user = 10,
        group = 20,
        domain = 30,
        anyone = 40
    }

    public enum PermissionRoles
    {
        owner = 10,
        reader = 20,
        organizer = 30,
        fileOrganizer = 40,
        writer = 50,
        commenter = 60,
    }

    public interface IPermission
    {
        string Id { get; set; }
        string FileId { get; set; }
        string GrantTo { get; set; }
        PermissionTypes Type { get; set; }
        PermissionRoles Role { get; set; }
        bool State { get; set; }
    }
}
