﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.FileHelper
{
    public class ContentFile : OnlineFile
    {
        public Stream Content { get; set; }
    }
}
