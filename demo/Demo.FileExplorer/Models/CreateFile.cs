﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.FileHelper
{
    public class CreateFile : IFile , IFileDetail
    {
        public string Name { get; set; }
        public string Id { get; set; }
        public string MimeType { get; set; }
        public IEnumerable<IFile> Parents { get; set; }
        public Dictionary<string, string> Properties { get; set; }
        public bool IsDirectory { get; set; }
        public string Owner { get; set; }
        public string Extension { get; set; }
        [JsonIgnore]
        public Stream Content { get; set; }
        public string UploadPath { get; set; }
        public string DisplayName { get; set; }
        
    }
}
