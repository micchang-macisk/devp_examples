﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.FileHelper
{
    public class IdFile:IFile
    {
        public IdFile() { }
        public IdFile(string FileId) { this.Id = FileId; }
        public string Name { get; set; }
        public string Id { get; set; }
        public string MimeType { get; set; }
        public bool IsDirectory { get; set; }

    }
}
