﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.FileHelper
{
    public class OnlineFile : IFile, IFileDetail
    {
        public string Name { get; set; }
        public string Id { get; set; }
        public string MimeType { get; set; }
        public IEnumerable<IFile> Parents { get; set; }
        public Dictionary<string, string> Properties { get; set; }
        public IEnumerable<string> Permissions { get; set; }
        public bool IsDirectory { get; set; }
        public string Owner { get; set; }
        public string Extension { get; set; }
    }
}
