﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.FileHelper
{

    public class Permission : IPermission
    {
        public string Id { get; set; }
        public string FileId { get; set; }
        public string GrantTo { get; set; }
        public PermissionTypes Type { get; set; }
        public PermissionRoles Role { get; set; }
        public bool State { get; set; }
    }
}
