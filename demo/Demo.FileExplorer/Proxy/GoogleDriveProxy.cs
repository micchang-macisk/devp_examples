﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Auth.OAuth2.Flows;
using Google.Apis.Auth.OAuth2.Responses;
using Google.Apis.Drive.v3;
using Google.Apis.Http;
using Google.Apis.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Demo.FileHelper.Proxy
{

    public class GoogleDriveProxy : FileProxy
    {
        public string Domain { get; set; }
        public string Account { get; set; }
        public string KeyFile { get; set; }

        public string ClientId { get; set; }
        public string ClientSecret { get; set; }

        public const string DirectoryMimeType = "application/vnd.google-apps.folder";
        //public const string DefaultFileFields = "id,name,mimeType,properties,parents";
        public const string DefaultFileFields = "*";
        public const string DefaultDirectoryAlias = "root";

        protected GoogleCredential ServiceCredential { get; set; }
        protected UserCredential UserCredential { get; set; }

        protected DriveService Service { get; set; }

        public enum CredentialType {
            ServiceAccount= 10 ,
            UserAccount = 20 

        }

        public GoogleDriveProxy()
        {
        }

        public GoogleDriveProxy(CredentialType InitialMode,ICredentialOption Option)
        {
            switch (InitialMode)
            {
                case CredentialType.ServiceAccount:
                    this.SetServiceAccount(Option);
                    break;
                case CredentialType.UserAccount:
                    this.SetUserAccount(Option);
                    break;
                default:
                    throw new Exception("must assign InitMode");
                    break;
            }

        }

        #region UserAccount 

        public FileProxy SetUserAccount(ICredentialOption CredentialOption)
        {
            try
            {
                var Option = (UserAccountOption)CredentialOption;
                this.Account = Option.Account;
                this.ClientId = Option.ClientId;
                this.ClientSecret = Option.ClientSecret;
                UserCredential = CreateUserCredential(Option);
                Service = InstanceDriveService(UserCredential);
                return this;
            }
            catch (Exception ex)
            {
                throw new Exception("建立連線發生錯誤!");
            }
        }

        public FileProxy SetUserAccount(string ClientId, string ClientSecret, string Account, string IdToken, string AccessToken, string RefreshToken)
        {
            return SetUserAccount(new UserAccountOption() {
                ClientId =ClientId,
                ClientSecret = ClientSecret,
                Account = Account,
                IdToken = IdToken,
                AccessToken = AccessToken,
                RefreshToken = RefreshToken,
            });
        }



        //protected UserCredential CreateUserCredential(string ClientId, string ClientSecret, string Account, string IdToken, string AccessToken, string RefreshToken)
        protected UserCredential CreateUserCredential(UserAccountOption Option)
        {
            Google.Apis.Auth.OAuth2.Flows.GoogleAuthorizationCodeFlow googleAuthFlow = new GoogleAuthorizationCodeFlow(new GoogleAuthorizationCodeFlow.Initializer()
            {
                ClientSecrets = new ClientSecrets()
                {
                    ClientId = Option.ClientId,
                    ClientSecret = Option.ClientSecret,
                },

            });
            Google.Apis.Auth.OAuth2.Responses.TokenResponse responseToken = new TokenResponse()
            {
                IdToken = Option.IdToken,
                AccessToken = Option.AccessToken,
                RefreshToken = Option.RefreshToken,
                Scope = "https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/drive.file https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/drive.appdata",
                TokenType = "Bearer",
            };
            var Credential = new UserCredential(googleAuthFlow, Option.Account, responseToken);
            Credential.RefreshTokenAsync(new System.Threading.CancellationToken());
            return Credential;
        }

        #endregion

        #region ServiceAccount

        public FileProxy SetServiceAccount(string Domain, string Account, string KeyFile)
        {
            try
            {
                SetServiceAccount(new ServiceAccountOption() { Domain= Domain, Account= Account, KeyFile= KeyFile }); 
                return this;
            }
            catch (Exception ex)
            {
                throw new Exception("建立連線發生錯誤!");
            }
        }
        public FileProxy SetServiceAccount(ICredentialOption CredentialOption)
        {
            try
            {
                var Option = (ServiceAccountOption)CredentialOption;
                this.Domain = Option.Domain;
                this.Account = Option.Account;
                this.KeyFile = Option.KeyFile;
                ServiceCredential = CreateServiceAccountCredential(Option);
                Service = InstanceDriveService(ServiceCredential);
                return this;
            }
            catch (Exception ex)
            {
                throw new Exception("建立連線發生錯誤!");
            }
        }


        //protected GoogleCredential CreateServiceAccountCredential(string Account, string KeyFile)
        protected GoogleCredential CreateServiceAccountCredential(ServiceAccountOption Option)
        {
            GoogleCredential Credential;
            using (Stream stream = new FileStream(Option.KeyFile, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                Credential = GoogleCredential.FromStream(stream);
            }

            string[] scopes = new[] { DriveService.Scope.DriveFile, DriveService.Scope.Drive };

            Credential = Credential.CreateScoped(scopes);

            return Credential;
        }



        #endregion

        protected DriveService InstanceDriveService(IConfigurableHttpClientInitializer Credential)
        {
            return new DriveService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = Credential,
                ApplicationName = "Document Management Service",
            });
        }

        public override void InitailCredential()
        {
            throw new NotImplementedException();
        }

        public override IFile GetDefaultDirectory()
        {
            return GetAsync(DefaultDirectoryAlias).Result;
        }
        public override IFile GetDirectory(string DirectoryId)
        {
            return new DriveCreateFile() { Id = DirectoryId };
        }

        #region Create
        public override Task<CreateFile> CreateAsync(CreateFile File, IEnumerable<IFile> Directories)
        {
            return Task<CreateFile>.Factory.StartNew(CreateTask, new { Service, File, Directories });
        }

        Func<object, CreateFile> CreateTask = (ArgObject) =>
        {
            var Args = (dynamic)ArgObject;
            var File = (CreateFile)Args.File;
            var Directories = (IEnumerable<IFile>)Args.Directories;
            var Service = (DriveService)Args.Service;
            using (var Stream = File.Content != null ? File.Content : new FileStream(File.UploadPath, FileMode.Open))
            {
                var DriveFile = new Google.Apis.Drive.v3.Data.File { Name = File.Name, MimeType = File.MimeType };
                if (!string.IsNullOrEmpty(File.DisplayName))
                {
                    if (DriveFile.AppProperties == null)
                        DriveFile.AppProperties = new Dictionary<string, string>();
                    DriveFile.AppProperties.Add("display_name", File.DisplayName);
                }
                    
                if (Directories.Count() > 0) DriveFile.Parents = Directories.Select(s => s.Id).ToList();
                var Creater = Service.Files.Create(DriveFile, Stream, File.MimeType);
                Creater.Fields = DefaultFileFields;
                Creater.ChunkSize = FilesResource.CreateMediaUpload.MinimumChunkSize * 2;                

                var Progress = Creater.UploadAsync();
                Progress.Wait();
                File.Id = Creater.ResponseBody.Id;
                File.Parents = Creater.ResponseBody.Parents.Select(s => new DriveCreateFile() { Id = s });
                return File;
            }
        };

        #endregion


        #region Folder
        public override Task<CreateDirectory> FolderAsync(CreateDirectory File, IEnumerable<IFile> Directories)
        {
            return Task<CreateDirectory>.Factory.StartNew(FolderTask, new { Service, File, Directories });
        }

        Func<object, CreateDirectory> FolderTask = (ArgObject) =>
        {
            var Args = (dynamic)ArgObject;
            var File = (CreateDirectory)Args.File;
            var Directories = (IEnumerable<IFile>)Args.Directories;
            var Service = (DriveService)Args.Service;

            var DriveFile = new Google.Apis.Drive.v3.Data.File { Name = File.Name, MimeType = File.MimeType };
            if (Directories.Count() > 0) DriveFile.Parents = Directories.Select(s => s.Id).ToList();
            var Creater = Service.Files.Create(DriveFile);
            Creater.Fields = DefaultFileFields;
            var Result = Creater.Execute();
            File.Id = Result.Id;
            return File;
        };

        #endregion

        #region Update
        public override Task<OnlineFile> UpdateAsync(OnlineFile File)
        {
            return Task<OnlineFile>.Factory.StartNew(UpdateTask, new { Service, File });
        }


        Func<object, OnlineFile> UpdateTask = (ArgObject) =>
        {
            var Args = (dynamic)ArgObject;
            var File = (OnlineFile)Args.File;
            var Service = (DriveService)Args.Service;
            var OriRequest = Service.Files.Get(File.Id);
            OriRequest.Fields = DefaultFileFields;
            OriRequest.Execute();
            if (OriRequest == null) return null;


            return File;
        };

        #endregion

        #region Delete
        public override Task<IFile> DeleteAsync(string FileId)
        {
            return Task<IFile>.Factory.StartNew(DeleteTask, new { Service, FileId });
        }


        Func<object, IFile> DeleteTask = (ArgObject) =>
        {
            var Args = (dynamic)ArgObject;
            var FileId = (string)Args.FileId;
            var Service = (DriveService)Args.Service;
            var OriRequest = Service.Files.Delete(FileId);
            OriRequest.Fields = DefaultFileFields;
            var Result = OriRequest.Execute();
            return new IdFile(Result);
        };

        #endregion

        #region List

        public override Task<IEnumerable<OnlineFile>> ListAsync(Condition Condition)
        {
            return Task<IEnumerable<OnlineFile>>.Factory.StartNew(ListTask, new { Service, Condition });
        }


        Func<object, IEnumerable<OnlineFile>> ListTask = (ArgObject) =>
        {
            var Args = (dynamic)ArgObject;
            var Condition = (Condition)Args.Condition;
            var Service = (DriveService)Args.Service;
            var Query = "";

            if (!string.IsNullOrEmpty(Condition.Query))
                Query = AppendToQuery(Query, string.Empty, Condition.Query);

            if (!string.IsNullOrEmpty(Condition.Keyword))
                Query = AppendToQuery(Query, "and", $"name contains '{Condition.Keyword}'");

            if (!string.IsNullOrEmpty(Condition.DirectoryId))
                Query = AppendToQuery(Query, "and", $"'{Condition.DirectoryId}' in parents");

            if (Condition.IsDirectory.HasValue)
                if (Condition.IsDirectory.Value)
                    Query = AppendToQuery(Query, "and", $"mimeType = '{DirectoryMimeType}'");
                else
                    Query = AppendToQuery(Query, "and", $"mimeType != '{DirectoryMimeType}'");

            if (Condition.IsShared == true)
                Query = AppendToQuery(Query, "and", $"sharedWithMe");

            var Result = new List<OnlineFile>();

            string PageToken = null;
            do
            {
                var OriRequest = Service.Files.List();
                OriRequest.Q = Query;
                OriRequest.Fields = $"nextPageToken, files({DefaultFileFields})";
                OriRequest.PageToken = PageToken;
                var OriResult = OriRequest.Execute();
                foreach (var OriFile in OriResult.Files)
                {
                    try
                    {
                        var FileMimeType = OriFile.MimeType.ToLower();
                        var OnlineFile = new DriveOnlineFile(OriFile);

                        if (Condition.IsWithDetail == true)
                        {

                            if (OriFile.Parents != null)
                                OnlineFile.Parents = OriFile.Parents.Select(s => new DriveOnlineId(s));
                        }

                        if (Condition.IsWithPermission == true)
                        {
                            //var PermissionRequest =  Service.Permissions.List(OnlineFile.Id);
                            //var PermissionList = PermissionRequest.Execute();
                            //PermissionList.
                        }

                        Result.Add(OnlineFile);
                    }
                    catch (Exception ex)
                    {
                        continue;
                    }
                }
                PageToken = OriResult.NextPageToken;
            } while (PageToken != null);

            return Result;
        };


        #endregion

        #region Get

        public override Task<OnlineFile> GetAsync(string FileId)
        {
            return Task<OnlineFile>.Factory.StartNew(GetTask, new { Service, FileId });
        }

        Func<object, OnlineFile> GetTask = (ArgObject) =>
        {
            var Args = (dynamic)ArgObject;
            var FileId = (string)Args.FileId;
            var Service = (DriveService)Args.Service;
            var OriRequest = Service.Files.Get(FileId);
            var File = OriRequest.Execute();
            return new DriveOnlineFile(File);
        };


        #endregion

        #region Move 

        public override Task<OnlineFile> MoveAsync(string FileId, string FolderId)
        {
            return Task<OnlineFile>.Factory.StartNew(MoveTask, new { Service, FileId, FolderId });
        }


        Func<object, OnlineFile> MoveTask = (ArgObject) =>
        {
            var Args = (dynamic)ArgObject;
            var FileId = (string)Args.FileId;
            var FolderId = (string)Args.FolderId;
            var Service = (DriveService)Args.Service;
            var OriRequest = Service.Files.Get(FileId);
            OriRequest.Fields = DefaultFileFields;
            var OriFile = OriRequest.Execute();
            var NewRequest = Service.Files.Update(new Google.Apis.Drive.v3.Data.File(), FileId);
            NewRequest.RemoveParents = (OriFile.Parents != null) ? string.Join(",", OriFile.Parents) : string.Empty;
            NewRequest.Fields = DefaultFileFields;
            NewRequest.AddParents = FolderId;
            var NewFile = NewRequest.Execute();
            return new DriveOnlineFile(NewFile);
        };

        #endregion

        public static string AppendToQuery(string Query, string Operator, string Append)
        {
            if (string.IsNullOrEmpty(Query)) return Append;
            return $"{Query} {Operator} {Append}";
        }

        #region Content

        public override Task<ContentFile> ContentAsync(string FileId)
        {
            return Task<ContentFile>.Factory.StartNew(ContentTask, new { Service, FileId });
        }

        Func<object, ContentFile> ContentTask = (ArgObject) =>
        {
            var Args = (dynamic)ArgObject;
            var FileId = (string)Args.FileId;
            var Service = (DriveService)Args.Service;
            var OriRequest = Service.Files.Get(FileId);
            OriRequest.Fields = DefaultFileFields;
            var OriFile = OriRequest.Execute();
            var File = new DriveContentFile(OriFile);
            File.Content = new MemoryStream();
            OriRequest.Download(File.Content);
            File.Content.Position = 0;
            return File;
        };

        #endregion

        public static IEnumerable<IFile> ParseToFiles(IEnumerable<string> FileIds)
        {
            return FileIds.Select(s => new DriveCreateFile() { Id = s });
        }

        #region Permission


        public override List<Task<IPermission>> CreatePermissionAsync(IEnumerable<IPermission> Permissions)
        {
            var Tasks = new List<Task<IPermission>>();
            foreach (var Permission in Permissions)
            {
                Task<IPermission> Task = null;
                if (Permission.Type == PermissionTypes.user)
                    Task = Task<IPermission>.Factory.StartNew(CreateUserPermissionTask, new { Service, Permission });
                else
                    Task = Task<IPermission>.Factory.StartNew(CreateDomainPermissionTask, new { Service, Permission });
                Tasks.Add(Task);
            }
            return Tasks;
        }


        Func<object, IPermission> CreateDomainPermissionTask = (ArgObject) =>
        {
            var Args = (dynamic)ArgObject;
            var Permission = (IPermission)Args.Permission;
            var Service = (DriveService)Args.Service;
            var domainPermission = new Google.Apis.Drive.v3.Data.Permission()
            {
                Type = "domain",
                Domain = Permission.GrantTo,
                Role = Permission.Role.ToString().ToLower(),
            };
            var Request = Service.Permissions.Create(domainPermission, Permission.FileId);
            Request.Fields = "*";
            var Response = Request.Execute();
            var Result = new DrivePermission(Permission.FileId, Response);
            return Result;
        };

        Func<object, IPermission> CreateUserPermissionTask = (ArgObject) =>
        {
            var Args = (dynamic)ArgObject;
            var Permission = (IPermission)Args.Permission;
            var Service = (DriveService)Args.Service;

            var userPermission = new Google.Apis.Drive.v3.Data.Permission()
            {
                Type = "user",
                EmailAddress = Permission.GrantTo,
                Role = Permission.Role.ToString().ToLower(),
            };
            var Request = Service.Permissions.Create(userPermission, Permission.FileId);
            Request.Fields = "*";
            var Response = Request.Execute();
            var Result = new DrivePermission(Permission.FileId, Response);
            return Result;
        };


        public override Task<IEnumerable<IPermission>> GetPermissionAsync(string FileId)
        {
            return Task<IEnumerable<IPermission>>.Factory.StartNew(GetPermissionTask, new { Service, FileId });
        }

        Func<object, IEnumerable<IPermission>> GetPermissionTask = (ArgObject) =>
        {
            var Args = (dynamic)ArgObject;
            var FileId = (string)Args.FileId;
            var Service = (DriveService)Args.Service;

            var Request = Service.Permissions.List(FileId);
            Request.Fields = "*";
            var Response = Request.Execute();
            return Response.Permissions.Select(s => new DrivePermission(FileId, s));
        };

        public override Task<IEnumerable<IPermission>> SyncPermission(IEnumerable<IPermission> Permissions, string FileId)
        {
            return Task<IEnumerable<IPermission>>.Factory.StartNew(SyncPermissionTask, new { Service, FileId, Permissions });
        }

        Func<object, IEnumerable<IPermission>> SyncPermissionTask = (ArgObject) =>
        {
            var Args = (dynamic)ArgObject;
            var FileId = (string)Args.FileId;
            var Permissions = (IEnumerable<IPermission>)Args.Permissions;
            var Service = (DriveService)Args.Service;
            var Result = new List<IPermission>();
            foreach (var Permission in Permissions)
            {
                var Request = Service.Permissions.Update(new Google.Apis.Drive.v3.Data.Permission(), FileId, Permission.Id);
                Request.Fields = "*";
                Result.Add(new DrivePermission(FileId, Request.Execute()));
            }
            return Result;
        };

        //protected Task<Permission> CreateDomainPermission(string FileId, PermissionRoles Role)
        //{
        //    Permission domainPermission = new Permission()
        //    {
        //        Type = "domain",
        //        Domain = Domain,
        //        Role = Role.ToString().ToLower(),
        //    };
        //    Request = Service.Permissions.Create(domainPermission, FileId);
        //    return Request.ExecuteAsync().Result;
        //}


        //protected Permission CreateUserPermission(string FileId, PermissionRoles Role, string Account)
        //{
        //    Permission domainPermission = new Permission()
        //    {
        //        Type = "user",
        //        Role = Role.ToString().ToLower(),
        //        EmailAddress = Account
        //    };
        //    Request = Service.Permissions.Create(domainPermission, FileId);
        //    return Request.ExecuteAsync().Result;
        //}




        #endregion
    }

    public class DrivePermission : Permission
    {
        public DrivePermission(IPermission Permission)
        {
            this.FileId = Permission.FileId;
            this.Role = Permission.Role;
            this.Type = Permission.Type;
            this.State = Permission.State;
            this.GrantTo = Permission.GrantTo;
            this.Id = Permission.Id;

        }
        public DrivePermission(string FileId, Google.Apis.Drive.v3.Data.Permission Permission)
        {
            this.FileId = FileId;
            this.Role = (PermissionRoles)Enum.Parse(typeof(PermissionRoles), Permission.Role);
            this.Type = (PermissionTypes)Enum.Parse(typeof(PermissionTypes), Permission.Type);
            this.State = true;
            this.GrantTo = Permission.Domain ?? Permission.EmailAddress;
            this.Id = Permission.Id;
            this.State = true;
        }

        public void Update(Google.Apis.Drive.v3.Data.Permission Permission)
        {
            this.Id = Permission.Id;
            this.State = true;
        }




    }

    public class DriveCreateFile : CreateFile
    {
        public DriveCreateFile()
        {

        }

        public DriveCreateFile(string FileId)
        {
            this.Id = FileId;
        }

        public DriveCreateFile(FileInfo File)
        {
            this.Name = File.Name;
            this.UploadPath = File.FullName;
            this.MimeType = MimeMapping.GetMimeMapping(File.Name);
        }



    }

    public class ServiceAccountOption : ICredentialOption
    {
        public string Domain { get; set; }
        public string Account { get; set; }
        public string KeyFile { get; set; }
        public string Root { get; set; }
    }


    public class UserAccountOption : ICredentialOption
    {
        public string ClientId { get; set; }
        public  string ClientSecret { get; set; }
        public  string Account { get; set; }
        public  string IdToken { get; set; }
        public  string AccessToken { get; set; }
        public string RefreshToken { get; set; }
        public string Root { get; set; }
    }

    public interface ICredentialOption {
         string Root { get; set; }
    }
    public class DriveCreateDirectory : CreateDirectory
    {
        public DriveCreateDirectory()
        {

        }

        public DriveCreateDirectory(string FolderName)
        {
            this.Name = FolderName;
            this.MimeType = GoogleDriveProxy.DirectoryMimeType;
        }
    }

    public class DriveOnlineFile : OnlineFile
    {
        public DriveOnlineFile() { }
        public DriveOnlineFile(string FileId) { this.Id = FileId; }

        public DriveOnlineFile(Google.Apis.Drive.v3.Data.File File)
        {
            this.Id = File.Id;
            this.Name = File.Name;
            this.MimeType = File.MimeType;
            this.IsDirectory = File.MimeType == GoogleDriveProxy.DirectoryMimeType;
        }
    }

    public class DriveOnlineId : IdFile
    {

        public DriveOnlineId(string FileId) { this.Id = FileId; }

    }

    public class DriveContentFile : ContentFile
    {
        public DriveContentFile(Google.Apis.Drive.v3.Data.File File)
        {
            this.Id = File.Id;
            this.Name = File.Name;
            this.MimeType = File.MimeType;
            this.IsDirectory = File.MimeType == GoogleDriveProxy.DirectoryMimeType;
        }

    }

}
