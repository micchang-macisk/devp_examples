﻿<%
For Each item In Request.Form
    Response.Write "Key: " & item & " - Value: " & Request.Form(item) & "<BR />"
    if instr(item,"fileupload_") then 
        var fileinfo = split(replace(item,"fileupload_"), "_")
        response.Write fileinfo(ubound(fileinfo))
        response.Write "<br />"
    end if
Next
For Each item In Request.QueryString
    Response.Write "Key: " & item & " - Value: " & Request.QueryString(item) & "<BR />"
Next
%>

<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8">   
        <META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
        <!-- IE可能不見得有效 -->
        <META HTTP-EQUIV="EXPIRES" CONTENT="0">
        <!-- 設定成馬上就過期 -->
        <link href="https://unpkg.com/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="https://unpkg.com/bootstrap-vue@latest/dist/bootstrap-vue.css" rel="stylesheet" type="text/css">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.2/css/all.min.css" rel="stylesheet" type="text/css">
        <link href="http://localhost/driveService/api/content/style" rel="stylesheet" type="text/css">
    </head>
    <body>
<form method="post" action="aspUpload.asp">
    
    <div id="uploadOne"></div>
    <input type="text" name="test" value="" />
    <input type="submit" value="送出" />
</form>
        

    <div id="test"></div>
        
        <script src="http://203.64.78.188/driveService/Scripts/jquery-1.10.2.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.16/vue.min.js"></script>
        <script src="https://unpkg.com/babel-polyfill@6.26.0/dist/polyfill.min.js"></script>
        <script src="https://unpkg.com/bootstrap-vue@2.0.0-rc.22/dist/bootstrap-vue.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.0/axios.min.js"></script>
        <script src="https://localhost/driveService/scripts/uploader.js"></script>

        <script type="text/javascript">

            
            $(function () {
                new Vue({el:"#test",template:"<div>123</div>"});
                let option = {
                    mode: "upload", // upload , list , both
                    templateId: "fileupload-template",
                    //templateUrl: "http://localhost/driveService/Content/fileupload-template.html",
                    templateUrl: "http://localhost/driveService/api/content/template",
                    //uploadUrl: "http://203.64.78.188/driveService/api/services/" + "a0001" + "/files",      
                    uploadUrl: "http://localhost/driveService/api/services/" + "a0001" + "/files",
                    el: "update_file",
                    //fileQueue:[{ name:"測試list",state:"seccuess",id:"" }],
                };
                loadUploader("#uploadOne", option);
            })
        </script>

        
<script type="text/x-template" id="fileupload-template">
    <div class="file-container">
      <div class="file-info" v-for="file in fileQueue" @click="viewFile(file)">
        <span class="file-info-title">          
          <span class="file-state">
            <span class="file-state-uploading"><i class='fas fa-upload' v-if="file.state=='uploading'"></i></span>
            <span class="file-state-checked"><i class='fas fa-check' v-if="file.state=='seccuess'"></i></span>
            <span class="file-state-error"><i class='fas fa-times' v-if="file.state=='error'"></i></span>
          </span>
          <span class="file-name">{{file.name}} </span>    
          <span class="file-message" v-if="file.state=='error'">{{file.stateDisplay}} 訊息：{{file.message}} </span>    
          <span class="file-message" v-if="file.state=='seccuess'">{{file.stateDisplay}}</span>    
        </span>
        <span class="file-info-operator">          
          <span class="file-view" @click="viewFile(file)"><i class='fas fa-eye' v-if="file.state=='seccuess'"></i></span>    
          <span class="file-delete" @click="removeFile(file)" v-if="canUpload"><i class='fas fa-trash-alt'></i></span>
        </span>
        <b-progress max="100" v-if="file.state!='seccuess' && file.state!='error'">
          <b-progress-bar :value="getProgreeNum(file)" :label="getProgreeDisplay(file)"></b-progress-bar>
        </b-progress>
        <span class="file-formdata" v-if="file.state=='seccuess'">
          <input type='text' :name='idFieldName' v-model="file.uploadId"  style="display:none;" >
          <input type='text' :name='nameFieldName' v-model="file.uploadName"  style="display:none;" >
          <input type='text' :name='displayFieldName' v-model="file.uploadDisplay" style="display:none;" >
        </span>
      </div>
      <input type="file" class="file-selector" ref="fileSelector" @change="fileSelected" multiple>
      <div class="select-panel" @click='openSelect' v-if="canUpload">
        <span class="action-tip">Drop file here or click to select file</span>
        <div class="upload-dropzone" style="display: none;visibility:hidden; opacity:0" id="dropzone">
          <div class="textnode">Drop file here to upload</div>
        </div>  
      </div>
      <div class="select-panel" v-if="emptyQueue">
        <span class="action-tip">Empty file list</span>
      </div>      
  </div>


</script>

    </body>
</html>