﻿using Demo.FileHelper;
using Demo.FileHelper.Proxy;
using Demo.Google.DriveApi.Helpers;
using Demo.Google.DriveApi.Models;
using Demo.Google.DriveApi.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Hosting;
using System.Web.Http;
using System.ComponentModel.DataAnnotations;
using static Demo.Google.DriveApi.Services.ExplorerService;
using System.Web.Http.Cors;

namespace Demo.Google.DriveApi.Controllers.Apis
{

    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ServiceController : ApiController
    {
        public ServiceController() {
            ExplorerService.SetCredentialOptions(new Dictionary<ProxyTypes, FileHelper.Proxy.ICredentialOption> {
                { ProxyTypes.ServiceAccountAtGoogleDriveProxy , new ServiceAccountOption(){
                     Domain = "gms.tcu.edu.tw" ,
                     Account = @"fileuploader@tcushoppingeform.iam.gserviceaccount.com" , 
                     KeyFile = HostingEnvironment.MapPath("~/App_Data/google.drive.api.json") ,
                     Root = "1FD4bo6Pf2Xljo0jmLcP-4yLVS_ghDk3b" ,
                } } ,
            });
        }

        #region Service Apply

        [Route("api/services"), HttpPost]
        public IHttpActionResult CreateService(ServiceInfo Info)
        {            
            try
            {
                using (var Service = new ApplyService())
                {
                    Service.CreateService(Info);
                    return Ok();
                }
            }
            catch (Exception ex)
            {
                return InternalServerError();
            }
        }

        [Route("api/services"), HttpGet]
        public IHttpActionResult ListServices()
        {
            try
            {
                using (var Service = new ApplyService())
                {
                    var Result = Service.ListServices();
                    return Ok(Result);
                }
            }
            catch (Exception ex)
            {
                return InternalServerError();
            }
        }
        
        [Route("api/services/{ServiceCode}"),HttpPut]
        public IHttpActionResult StopService([FromUri]string ServiceCode)
        {
            try
            {
                using (var Service = new ApplyService())
                {
                    Service.StopService(ServiceCode);
                    return Ok();
                }
            }
            catch (Exception ex)
            {
                return InternalServerError();
            }
        }

        [Route("api/services/{ServiceCode}"),HttpDelete]
        public IHttpActionResult DeleteService(string ServiceCode)
        {
            try
            {
                using (var Service = new ApplyService())
                {
                    Service.DeleteService(ServiceCode);
                    return Ok();
                }
            }
            catch (Exception ex)
            {
                return InternalServerError();
            }
        }

        #endregion

        #region File Explorer

        [Route("api/services/{ServiceCode}/files"), HttpPost]
        public IHttpActionResult PostFile(string ServiceCode)
        {
            using (var Service = new ExplorerService(ServiceCode))
            using (var ApplyService = new ApplyService())
            {
                try
                {

                    var Files = GetMultiPartFiles(Request);
                    ApplyService.CheckFiles(ServiceCode, Files);
                    var Result = Service.CreateFiles(Files);
                    ApplyService.LogFiles(ServiceCode,Files);
                   
                    //var Result = Service.ListFiles();
                    return Ok(Result);
                }
                catch (Exception ex)
                {
                    return InternalServerError(ex);
                }
            }
        }


        #region Multi File Content
        /// <summary>
        /// 解析Multipart訊息成為檔案物件
        /// </summary>
        /// <param name="Request"></param>
        /// <returns></returns>
        private IEnumerable<CreateFile> GetMultiPartFiles(HttpRequestMessage Request)
        {
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            var Provider = new MultipartFileProvider<CreateFile>()
            {
                TranslateModel = FileContentHandler,
                GetContentStream = FileStreamHandler
            };

            var Task = Request.Content.ReadAsMultipartAsync(Provider);
            Task.Wait();

            var Result = new List<CreateFile>();
            foreach (var Content in Provider.DataContent)
            {
                if (!(Content is CreateFile)) continue;
                Content.Extension = new System.IO.FileInfo(Content.Name).Extension;
                Result.Add(Content);
            }
            return Result;
        }

        /// <summary>
        /// 自訂取得檔案相關資訊
        /// </summary>
        protected Func<HttpContentHeaders, CreateFile> FileContentHandler = (Headers) => new CreateFile
        {
            MimeType = Headers.ContentType.MediaType,
            Name = MultipartFileProvider<CreateFile>.ParseFileName(Headers),
            Content = new MemoryStream(),
            Id = Headers.ContentDisposition.Name
        };

        /// <summary>
        /// 自訂取得檔案內容
        /// </summary>
        protected Func<CreateFile, Stream> FileStreamHandler = (Content) => Content.Content;


        #endregion

        [Route("api/services/{ServiceCode}/files"), HttpGet]
        public IHttpActionResult ListFiles(string ServiceCode)
        {
            using (var Service = new ExplorerService(ServiceCode))
            {
                var Result = Service.ListFiles();
                return Ok(Result);
            }
        }

        [Route("api/services/{ServiceCode}/files/{FileId}"), HttpDelete]
        public IHttpActionResult DeleteFile(string ServiceCode, string FileId)
        {
            using (var Service = new ExplorerService(ServiceCode))
            {
                Service.DeleteFile(FileId);
                return Ok();
            }
        }
        
        [Route("api/services/{ServiceCode}/files/{FileId}"), HttpGet]
        public HttpResponseMessage GetFile(string ServiceCode, string FileId)
        {
            using (var Service = new ExplorerService(ServiceCode))
            {
                var File = Service.GetFileContent(FileId);
                var ResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
                ResponseMessage.Content = new StreamContent(File.Content);
                ResponseMessage.Content.Headers.ContentType = new MediaTypeHeaderValue(File.MimeType);
                ResponseMessage.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                {
                    FileName = File.Name
                };
                return ResponseMessage;
            }
        }

        #endregion

    }
}
