﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Demo.Google.DriveApi.Controllers.Apis
{

    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ContentController : ApiController
    {


        [Route("api/content/template"), HttpGet]
        public HttpResponseMessage GetTemplate()
        {
            try
            {

                var Result = Request.CreateResponse(HttpStatusCode.OK);
                Result.Content = new StringContent(File.ReadAllText(HttpContext.Current.Server.MapPath("~/Content/fileupload-template.html")), Encoding.UTF8, "text/html");
                return Result;

            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK);
            }
        }

        
        [Route("api/content/style"), HttpGet]
        public HttpResponseMessage GetStyle()
        {
            try
            {

                var Result = Request.CreateResponse(HttpStatusCode.OK);
                Result.Content = new StringContent(File.ReadAllText(HttpContext.Current.Server.MapPath("~/Content/uploader.css")), Encoding.UTF8, "text/css");
                return Result;
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK);
            }
        }

    }
}
