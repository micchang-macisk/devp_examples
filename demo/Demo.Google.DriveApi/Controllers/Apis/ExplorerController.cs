﻿using Demo.FileHelper;
using Demo.FileHelper.Extensions;
using Demo.FileHelper.Proxy;
using Demo.Google.DriveApi.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Hosting;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Demo.Google.DriveApi.Controllers.Apis
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("api/explorer")]
    public class ExplorerController : ApiController
    {
        private const string Domain = "gms.tcu.edu.tw";
        private const string Account = @"fileuploader@tcushoppingeform.iam.gserviceaccount.com";
        private string KeyFile = HostingEnvironment.MapPath("~/App_Data/google.drive.api.json");
        private string[] UpFiles = new string[] { HostingEnvironment.MapPath("~/App_Data/error_cancel.png"), HostingEnvironment.MapPath("~/App_Data/iconfinder_icon_animal_cachorro_3316536.png") };
        private string AdminAccount = "tcushoppingeform@gms.tcu.edu.tw";

        [Route("upload"), HttpPost]
        public IHttpActionResult Upload(string ServiceFile = "~/App_Data/iconfinder_icon_animal_cachorro_3316536.png", string FolderId = null)
        {
            var Explorer = new GoogleDriveProxy()
                .SetServiceAccount(Domain, Account, KeyFile)
                .GetExplorer();
            if (!string.IsNullOrEmpty(FolderId))
                Explorer.ChangeToDirectory(FolderId);
            var FileInfo = new FileInfo(HostingEnvironment.MapPath(ServiceFile));
            var Uploads = new DriveCreateFile(FileInfo).ToList<CreateFile>();            
            var Files = Explorer.Create(Uploads);
            var Permission = Explorer.CreateDomainPermission(Domain, Files.Select(s => "," + s.Id).FirstOrDefault(), PermissionRoles.reader).ToList();
            var ParentPermissions = Explorer.GetPermission(FolderId);
            var userPermission = ParentPermissions.Where(c => c.GrantTo == AdminAccount);
            Explorer.SyncPermission(Files.Select(s=>s.Id),userPermission);
            return Ok(Files);
        }

        [Route("folder"), HttpPost]
        public IHttpActionResult Folder(string CreateName, string FolderId=null)
        {
            var Explorer = new GoogleDriveProxy()
                .SetServiceAccount(Domain, Account, KeyFile)
                .GetExplorer();
            Explorer.ChangeToDirectory(FolderId);
            var Uploads = new DriveCreateDirectory(CreateName).ToList<CreateDirectory>();
            var Files = Explorer.Folder(Uploads);
            var Permission = Explorer.CreateDomainPermission(Domain, Files.Select(s => "," + s.Id).FirstOrDefault(), PermissionRoles.reader).ToList();
            //Permission.AddRange(Explorer.CreateUserPermission("huwyang@macisk.idv.tw", Files.Select(s => "," + s.Id).FirstOrDefault(), PermissionRoles.writer));
            return Ok(Files);
        }

        [Route("move"), HttpPut]
        public IHttpActionResult Move(string FileIds, string FolderId)
        {
            var Explorer = new GoogleDriveProxy()
                .SetServiceAccount(Domain, Account, KeyFile)
                .GetExplorer();

            var Files = Explorer.Move(FileIds, FolderId);
            return Ok(Files);
        }

        //[Route("update"), HttpGet]
        //public IHttpActionResult Update(string FileIds = null)
        //{
        //    var Explorer = new GoogleDriveFileProxy()
        //        .SetServiceAccount(Domain, Account, KeyFile)
        //        .GetExplorer();
        //    return Ok();
        //}

        [Route("delete"), HttpDelete]
        public IHttpActionResult Delete(string FileIds = null)
        {
            var Explorer = new GoogleDriveProxy()
                .SetServiceAccount(Domain, Account, KeyFile)
                .GetExplorer();
            var Files = Explorer.Delete(FileIds);
            return Ok(Files);
        }

        [Route("list"), HttpGet]
        public IHttpActionResult List([FromUri]Condition Condition = default(Condition))
        {
            var User = new UserService().GetUserByEmail(AdminAccount);
            var Explorer = new GoogleDriveProxy()
                .SetServiceAccount(Domain, Account, KeyFile)
                .GetExplorer();

            if (Condition == null)
                Condition = new Condition();

            var Files = Explorer.List(Condition);
            return Ok(Files);
        }

        [Route("get"), HttpGet]
        public IHttpActionResult Get(string FileIds)
        {
            var Explorer = new GoogleDriveProxy()
                .SetServiceAccount(Domain, Account, KeyFile)
                .GetExplorer();

            var Files = Explorer.Get(FileIds);
            return Ok(Files);
        }

        [Route("content/{fileid}"), HttpGet]
        public HttpResponseMessage Content(string FileId)
        {
            var Explorer = new GoogleDriveProxy()
                .SetServiceAccount(Domain, Account, KeyFile)
                .GetExplorer();
            var File = Explorer.Content(FileId).FirstOrDefault();
            var ResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            ResponseMessage.Content = new StreamContent(File.Content);
            ResponseMessage.Content.Headers.ContentType = new MediaTypeHeaderValue(File.MimeType);
            return ResponseMessage;
        }

        [Route("permission"), HttpPost]
        public IHttpActionResult CreatePermission(string Accounts,string FileIds)
        {
            var Explorer = new GoogleDriveProxy()
                .SetServiceAccount(Domain, Account, KeyFile)
                .GetExplorer();
            return Ok();
        }

        [Route("permission/{fileids}"), HttpPut]
        public IHttpActionResult DeletePermission(string Accounts, string FileIds)
        {
            var Explorer = new GoogleDriveProxy()
                .SetServiceAccount(Domain, Account, KeyFile)
                .GetExplorer();

            return Ok();
        }


        [Route("permission"), HttpGet]
        public IHttpActionResult GetPermission(string FileIds)
        {
            var Explorer = new GoogleDriveProxy()
                .SetServiceAccount(Domain, Account, KeyFile)
                .GetExplorer();
            var Permissions = Explorer.GetPermission(FileIds);
            return Ok(Permissions);
        }

        [Route("remove"), HttpDelete]
        public IHttpActionResult ListDelete([FromUri]Condition Condition = default(Condition))
        {
            var Explorer = new GoogleDriveProxy()
                .SetServiceAccount(Domain, Account, KeyFile)
                .GetExplorer();

            if (Condition == null)
                Condition = new Condition();

            var Files = Explorer.List(Condition);
            var FileIds = Files.Select(s => s.Id);
            var Deletes = Explorer.Delete(FileIds);
            Files = Explorer.List(Condition);
            return Ok(Files);
        }

    }
}
