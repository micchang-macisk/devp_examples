﻿using Demo.FileHelper;
using Demo.FileHelper.Proxy;
using Demo.Google.DriveApi.Helpers;
using Demo.Google.DriveApi.Models;
using Demo.Google.DriveApi.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Hosting;
using System.Web.Http;

namespace Demo.Google.DriveApi.Controllers.Apis
{
    public class ServiceController : ApiController
    {
        public ServiceController() {
            ExplorerService.SetCredentialOptions(new Dictionary<string, FileHelper.Proxy.ICredentialOption> {
                { ExplorerService.GoogleDriveServiceAccountProxyType , new ServiceAccountOption(){
                     Domain = "macisk.idv.tw" ,
                     Account = @"google-drive-api@labs-212003.iam.gserviceaccount.com" , 
                     KeyFile = HostingEnvironment.MapPath("~/App_Data/google.drive.api.json") ,
                } } ,
            });
        }
                
        [Route("api/services"), HttpPost]
        public IHttpActionResult CreateService(ServiceInfo Service)
        {
            try
            {
                using (var Apply = new ApplyService())
                {
                    Apply.CreateService(Service);
                    return Ok();
                }
            }
            catch (Exception ex)
            {
                return InternalServerError();
            }
        }

        [Route("api/services"), HttpGet]
        public IHttpActionResult ListServices()
        {
            try
            {
                using (var Apply = new ApplyService())
                {
                    var Result = Apply.ListServices();
                    return Ok(Result);
                }
            }
            catch (Exception ex)
            {
                return InternalServerError();
            }
        }
        
        [Route("api/services/{ServiceId}"), HttpPut]
        public IHttpActionResult StopService(int ServiceId)
        {
            try
            {
                using (var Apply = new ApplyService())
                {
                    Apply.StopService(ServiceId);
                    return Ok();
                }
            }
            catch (Exception ex)
            {
                return InternalServerError();
            }
        }

        [Route("api/services/{ServiceId}"), HttpDelete]
        public IHttpActionResult DeleteService(int ServiceId)
        {
            try
            {
                using (var Apply = new ApplyService())
                {
                    Apply.DeleteService(ServiceId);
                    return Ok();
                }
            }
            catch (Exception ex)
            {
                return InternalServerError();
            }
        }

        [Route("api/services/{ServiceId}/files"), HttpPost]
        public IHttpActionResult PostFile([FromUri]int ServiceId)
        {
            using (var Explorer = new ExplorerService(ServiceId))
            {
                var Files = GetMultiPartFiles(Request);

                return Ok();
            }
        }

        private IEnumerable<CreateFile> GetMultiPartFiles(HttpRequestMessage Request)
        {
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            var provider = new MultipartFileProvider<CreateFile>() {
                TranslateModel = (Headers) => new CreateFile {
                     MimeType = Headers.ContentType.MediaType ,
                     Name = MultipartFileProvider<CreateFile>.ParseFileName(Headers) ,
                     Content = new MemoryStream(),
                     Id = Headers.ContentDisposition.Name
                },
                GetContentStream = (Content) => Content.Content
            };

            var Task = Request.Content.ReadAsMultipartAsync(provider);
            Task.Wait();

            foreach (var Content in provider.DataContent)
            {
                if (Content is CreateFile)
                    yield return Content;
            }
        }

        

        [Route("api/services/{ServiceId}/files"), HttpGet]
        public IHttpActionResult ListFiles(int ServiceId)
        {
            using (var Explorer = new ExplorerService(ServiceId))
            {
                var Result = Explorer.ListFIles();
                return Ok(Result);
            }
        }

        [Route("api/services/{ServiceId}/files/{FileId}"), HttpDelete]
        public IHttpActionResult DeleteFile(int ServiceId,int FileId)
        {
            using (var Explorer = new ExplorerService(ServiceId))
            {

                return Ok();
            }
        }
        
        [Route("api/services/{ServiceId}/files/{FileId}"), HttpGet]
        public IHttpActionResult GetFile(int ServiceId, int FileId)
        {
            using (var Explorer = new ExplorerService(ServiceId))
            {

                return Ok();
            }
        }

    }
}
