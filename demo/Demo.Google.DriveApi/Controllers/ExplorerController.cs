﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Demo.Google.DriveApi.Controllers
{
    public class ExplorerController : Controller
    {
        // GET: Explorer
        public ActionResult Start()
        {
            return View();
        }

        public ActionResult TempOne()
        {
            return View();
        }

        public ActionResult TempTwo()
        {
            return View();
        }

        public ActionResult Template(string ServiceCode) {
            return View();
        }
    }
}