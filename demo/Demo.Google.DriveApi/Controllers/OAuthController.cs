﻿using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;

namespace Demo.Google.DriveApi.Controllers
{
    public class OAuthController : Controller
    {

//        private string CallBack = HostingEnvironment.MapPath("~/App_Data/google.drive.api.json");

        [Route("oauth/drive/start")]
        // GET: Drive
        public ActionResult Start()
        {
            return View();
        }

        [Route("oauth/drive/authencate")]
        // GET: Drive
        public ActionResult Authencate(string ReplyUrl=null)
        {
            return (ActionResult)new ChallengeResult("Google", ReplyUrl ?? $"/oauth/drive/identify");
        }

        [Route("oauth/drive/identify")]
        // GET: Drive
        public ActionResult Identify()
        {
            return View();
        }
    }

    public class ChallengeResult : HttpUnauthorizedResult
    {
        public ChallengeResult(string provider, string redirectUri)
            : this(provider, redirectUri, null)
        {
        }

        public ChallengeResult(string provider, string redirectUri, string userId)
        {
            LoginProvider = provider;
            RedirectUri = redirectUri;
            UserId = userId;
        }

        public string LoginProvider { get; set; }
        public string RedirectUri { get; set; }
        public string UserId { get; set; }
        // 新增外部登入時用來當做 XSRF 保護
        private const string XsrfKey = "XsrfId";

        public override void ExecuteResult(ControllerContext context)
        {
            var properties = new AuthenticationProperties { RedirectUri = RedirectUri };

            if (UserId != null)
            {
                properties.Dictionary[XsrfKey] = UserId;
            }
            context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
        }

    }
}