﻿using Newtonsoft.Json;
using Demo.Google.DriveApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace Demo.Google.DriveApi.Controllers
{

    public class OriginalController : Controller
    {
        // GET: oauth/Original
        [Route("oauth/original/start")]
        public ActionResult Start()
        {
            return View();
        }

        [Route("oauth/original/oauth")]
        public ActionResult OAuth(string ClientId, string ResponseType, string Scope, string RedirectUrl, string State)
        {
            var RedUrl = $"https://accounts.google.com/o/oauth2/v2/auth?client_id={ClientId}&response_type={ResponseType}&scope={Server.UrlEncode(Scope)}&redirect_uri={Server.UrlEncode(RedirectUrl)}&state={State}";
            return Redirect(RedUrl);
        }

        [Route("oauth/original/callback")]
        public ActionResult CallBack()
        {
            return View();
        }

        [Route("oauth/original/exchange")]
        public ActionResult Exchange(string State, string Code, string Scope)
        {
            using (var client = new HttpClient())
            {
                var IdCodeDisplay = new IdCodeDisplay();
                var Result = client.PostAsJsonAsync("https://www.googleapis.com/oauth2/v4/token", new
                {
                    code = Code,
                    client_id = Constants.ClientId,
                    client_secret = Constants.ClientSecret,
                    redirect_uri = $"http://localhost:63293{Url.Action("CallBack").ToLower()}",
                    grant_type = "authorization_code",
                    access_type = "offline"
                }).Result.Content.ReadAsStringAsync().Result;
                IdCodeDisplay.IdCode = JsonConvert.DeserializeObject<IdCode>(Result);
                IdCodeDisplay.ResponseContent = JsonConvert.SerializeObject(IdCodeDisplay.IdCode, Formatting.Indented);
                return View(IdCodeDisplay);
            }
        }

        [Route("oauth/original/idtokeninfo")]
        public ActionResult IdTokenInfo(string id_token)
        {
            using (var client = new HttpClient())
            {
                var IdTokenDisplay = new IdTokenDisplay();
                var Result = client.GetStringAsync("https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=" + id_token).Result;
                IdTokenDisplay.TokenInfo = JsonConvert.DeserializeObject<TokenInfo>(Result);
                IdTokenDisplay.ResponseContent = JsonConvert.SerializeObject(IdTokenDisplay.TokenInfo, Formatting.Indented);
                return View(IdTokenDisplay);
            }
        }


        [Route("oauth/original/userinfo")]
        public ActionResult UserInfo()
        {
            return View();
        }
    }

}