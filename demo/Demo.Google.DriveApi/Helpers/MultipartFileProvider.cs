﻿using Demo.FileHelper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;

namespace Demo.Google.DriveApi.Helpers
{

    public class MultipartFileProvider<TFile> : MultipartStreamProvider where TFile :  new()
    {
        public MultipartFileProvider() { DataContent = new List<TFile>(); }
        public List<TFile> DataContent { get; set; }
        public override Stream GetStream(HttpContent Parent, HttpContentHeaders Headers)
        {
            if (Parent == null)
            {
                throw new ArgumentNullException(nameof(Parent));
            }

            TFile Content = default(TFile);
            if (Headers.ContentType != null)
                Content = TranslateModel(Headers);
            //else
            //    Content = DefaultModel(Headers);
            
            DataContent.Add(Content);
            return GetContentStream(Content);
        }
        public Func<HttpContentHeaders, TFile> TranslateModel;
        public Func<HttpContentHeaders, TFile> DefaultModel;
        public Func<TFile, Stream> GetContentStream;


        public static string ParseFileName(HttpContentHeaders Headers)
        {
            string FileName = null;
            var ContentDisposition = Headers.ContentDisposition;
            if (ContentDisposition == null)
            {
                FileName = null;
                return null;
            }

            FileName = UnquoteToken(ContentDisposition.FileName);
            return FileName;
        }

        public static string UnquoteToken(string Token)
        {
            if (string.IsNullOrWhiteSpace(Token))
                return Token;

            if (Token.StartsWith("\"", StringComparison.Ordinal) && Token.EndsWith("\"", StringComparison.Ordinal) && Token.Length > 1)
                return Token.Substring(1, Token.Length - 2);

            return Token;
        }
    }
}