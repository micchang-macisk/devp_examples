﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace Demo.Google.DriveApi.Helpers
{
    public class SwaggerAuthenticationHandler : DelegatingHandler
    {
        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            var IsSwaggerRequest = request.RequestUri.Segments.Contains("swagger");
            var Credential = request.Headers.GetCookies("authencation");
            var IsUnAuthencate = Credential.Count == 0;

            if (IsSwaggerRequest && IsUnAuthencate)
            {
                HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.Redirect);
                var Authority = $"{request.RequestUri.Scheme}://{request.RequestUri.Authority}";
                string uri = $"{Authority}/oauth/drive/authencate?ReplyUrl={Authority}/swagger";
                response.Headers.Location = new Uri(uri);
                return Task.FromResult(response);
            }
            return base.SendAsync(request, cancellationToken);
        }
    }
}