﻿using Demo.Google.DriveApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Demo.Google.DriveApi
{
    public static class Identifies
    {
        public static User User {
            get {
                try
                {
                    return (User)HttpContext.Current.Session["User"];
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
            set {
                HttpContext.Current.Session["User"] = value;
            }
        }

    }
}