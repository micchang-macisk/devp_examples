﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Demo.Google.DriveApi.Models
{
    public class User
    {
        public string Email { get; set; }

        public string Id { get; set; }

        public string AccessToken { get; set; }

        public string RefreshToken { get; set; }
    }
}