﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Demo.Google.DriveApi.Models
{

    public class OAuthUser
    {
        public string Id { get; set; }
        public string User { get; set; }
        public string Email { get; set; }

    }

    public class IdCodeDisplay
    {

        public string ResponseContent { get; set; }
        public IdCode IdCode { get; set; }

    }

    public class IdCode
    {
        public string access_token { get; set; }
        public int expires_in { get; set; }
        public string scope { get; set; }
        public string token_type { get; set; }
        public string id_token { get; set; }
    }

    public class IdTokenDisplay
    {

        public string ResponseContent { get; set; }

        public TokenInfo TokenInfo { get; set; }
    }

    public class TokenInfo
    {
        public string iss { get; set; }
        public string azp { get; set; }
        public string aud { get; set; }
        public string sub { get; set; }
        public string hd { get; set; }
        public string email { get; set; }
        public string email_verified { get; set; }
        public string at_hash { get; set; }
        public string name { get; set; }
        public string picture { get; set; }
        public string given_name { get; set; }
        public string family_name { get; set; }
        public string locale { get; set; }
        public string iat { get; set; }
        public string exp { get; set; }
        public string alg { get; set; }
        public string kid { get; set; }
        public string typ { get; set; }
    }
}