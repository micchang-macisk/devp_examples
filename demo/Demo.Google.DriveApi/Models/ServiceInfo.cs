﻿using Demo.Google.DriveApi.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static Demo.Google.DriveApi.Services.ExplorerService;

namespace Demo.Google.DriveApi.Models
{
    public class ServiceInfo
    {
        public ServiceInfo()
        {

        }

        public ServiceInfo(FileService DbService)
        {
            this.Id = DbService.Serv_Id;
            this.Code = DbService.Serv_Code;
            this.Proxy = DbService.Serv_Proxy;
            this.Path = DbService.Serv_Path;
            this.Memo = DbService.Serv_Memo;
            this.Owner = DbService.Serv_Owner;
            this.CreateDate = DbService.Serv_CreateDate;
            this.UpdateDate = DbService.Serv_UpdateDate;

        }

        public ProxyTypes ProxyType {
            get {
                return (ProxyTypes)Enum.Parse(typeof(ProxyTypes), this.Proxy);
            }
        }

        public int Id { get; set; }
        public string Code { get; set; }

        public string Extentions { get; set; }
        public int Sizes { get; set; }

        public string Exts { get; set; }
        public int Size { get; set; }
        public string Proxy { get; set; }
        public string Path { get; set; }

        public string Memo { get; set; }

        public string Owner { get; set; }

        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }


    }
}