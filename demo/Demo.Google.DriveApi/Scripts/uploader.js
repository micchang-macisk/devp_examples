﻿function dumpComputedStyles(elem, prop) {

    var cs = window.getComputedStyle(elem, null);
    if (prop) {
        console.log(prop + " : " + cs.getPropertyValue(prop));
        return;
    }
    var len = cs.length;
    for (var i = 0; i < len; i++) {

        var style = cs[i];
        console.log(style + " : " + cs.getPropertyValue(style));
    }

}

function initialUploader(id, option) {
    return new Vue({
        el: id,
        template: "#" + option.templateId,
        props: ['FileApi'],
        data: {
            fileApi: option.uploadUrl,
            content: "123456",
            uploadPanel: null,
            fileQueue: option.fileQueue ? option.fileQueue : [],
            option: option,
            tagId: id,
        },
        computed: {
            emptyQueue: function () { return this.option.mode == "list" && this.fileQueue.length == 0; },
            canUpload: function () { return this.option.mode != "list"; },
            fileApi: function () { return this.option.uploadUrl; },
            idFieldName: function () { return this.option.el + "_id"; },
            nameFieldName: function () { return this.option.el + "_name"; },
            displayFieldName: function () { return this.option.el + "_display"; },
        },
        methods: {
            getFileDisplay: function (file) { return file.uploadId },
            fileSelected: function () {
                this.uploadFiles(this.$refs.fileSelector.files);
            },
            openSelect: function () {
                this.$el.querySelector(".file-selector").click();
            },
            viewFile: function (file) {
                window.open(file.url, '_new');
            },
            removeFile: function (file) {
                window.event.preventDefault();
                window.event.stopPropagation();

                var fIdx = this.fileQueue.indexOf(file);
                this.fileQueue.splice(fIdx, 1);
            },
            getProgreeNum: function (file) {
                if (file.state == "error") return 0;
                return file.progreeNum;
            },
            getProgreeDisplay: function (file) {
                if (file.state == "error") return "0%";
                if (file.state == "seccuess") return "上傳完成";
                if (file.state == "uploading" && file.progreeNum == "100") return "同步檔案中";
                if (file.state == "uploading") return file.progreeNum + "%上傳中";
                if (file.message) return "訊息" + file.message;
                return "";
            },
            queueFiles: function (files) {
                var self = this;
                for (var fIdx = 0; fIdx < files.length; fIdx++) {
                    var file = files[fIdx];
                    file.uploadNo = this._uuid();
                    file.progreeNum = 0;
                    file.state = "uploading";
                    file.stateDisplay = "上傳中";
                    file.url = "http://www.gooogle.com";
                    file.uploadId = "";
                    file.uploadName = "";
                    file.uploadDisplay = "";
                    file.message = "";
                    this.fileQueue.push(file);
                }
            },
            uploadFiles: function (files) {
                this.queueFiles(files);
                this.upload(files);
            },
            upload: function (files) {

                var self = this;
                for (var fIdx = 0; fIdx < files.length; fIdx++) {
                    let file = files[fIdx];

                    var url = self.fileApi;
                    var xhr = new XMLHttpRequest();
                    var formData = new FormData();
                    xhr.open('POST', url, true);
                    xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');

                    // Update progress (can be used to show progress indicator)
                    xhr.upload.addEventListener("progress", function (e) {
                        file.progreeNum = (e.loaded * 100.0 / e.total || 100);
                        file.state = "uploading";
                        file.stateDisplay = "上傳中";
                        self.$forceUpdate();
                    });

                    // Update progress (can be used to show progress indicator)
                    xhr.upload.addEventListener("error", function (e) {
                        file.progreeNum = 0;
                        file.state = "error";
                        file.stateDisplay = "發生錯誤";
                        file.message = "無法連接伺服器!";
                        self.$forceUpdate();
                    });

                    xhr.addEventListener('readystatechange', function (e) {
                        var xhr = e.currentTarget;
                        if (xhr.readyState == 4 && xhr.status == 200) {
                            file.progreeNum = 100;
                            file.state = "seccuess";
                            file.stateDisplay = "已上傳";
                            let fileInfos = JSON.parse(xhr.response);
                            if (fileInfos.length == 0) {
                                file.progreeNum = 0;
                                file.state = "error";
                                file.stateDisplay = "發生錯誤";
                                file.message = "接收上傳結果時發生錯誤";
                            }
                            let fileInfo = fileInfos[0];
                            self.$set(file, "uploadId", fileInfo.Id);
                            self.$set(file, "uploadName", fileInfo.Name);
                            self.$set(file, "uploadDisplay", fileInfo.DisplayName);
                            //file.uploadId = fileInfo.Id;
                            //file.uploadName = fileInfo.Name;
                            //file.uploadDisplay = fileInfo.DisplayName
                            self.$forceUpdate();
                        }
                        else if (xhr.readyState == 4 && xhr.status != 200) {
                            file.progreeNum = 0;
                            file.state = "error";
                            file.stateDisplay = "發生錯誤";
                            file.message = "伺服器發生錯誤";
                            self.$forceUpdate();
                            console.log("upload error");
                        }
                    });

                    //formData.append('upload_preset', 'ujpu6gyk');
                    formData.append('file', file);
                    xhr.send(formData);


                }
            },
            showPanel: function (panel, tip) {
                panel.style.display = "table";
                panel.style.visibility = "visible";
                panel.style.opacity = 1;
                panel.style.fontSize = "48px";
            },
            hidePanel: function (panel, tip) {
                panel.style.display = "none";
                panel.style.visibility = "hidden";
                panel.style.opacity = 0.2;
                panel.style.fontSize = "42px";
            },
            _uuid: function () {
                function s4() {
                    return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
                }
                return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
            },
        },
        updated: function () {

        },
        mounted: function () {

            window.addEventListener("dragenter", function (e) { e.preventDefault(); e.stopPropagation(); });
            window.addEventListener("dragleave", function (e) { e.preventDefault(); e.stopPropagation(); });
            window.addEventListener("dragover", function (e) { e.preventDefault(); e.stopPropagation(); });
            window.addEventListener("drop", function (e) { e.preventDefault(); e.stopPropagation(); });

            if (!this.canUpload) return;

            var self = this;

            self.uploadPanel = $(self.$el).find(".upload-dropzone")[0];
            self.actionTip = $(self.$el).find(".action-tip")[0];

            this.$el.addEventListener("dragenter", function (e) {
                e.preventDefault();
                e.stopPropagation();
                self.showPanel(self.uploadPanel, self.actionTip);
            });

            this.$el.addEventListener("dragleave", function (e) {
                e.preventDefault();
                e.stopPropagation();
                self.hidePanel(self.uploadPanel, self.actionTip);
            });

            this.$el.addEventListener("dragover", function (e) {
                e.preventDefault();
                e.stopPropagation();
                self.showPanel(self.uploadPanel, self.actionTip);
            });

            this.$el.addEventListener("drop", function (e) {
                e.preventDefault();
                e.stopPropagation();
                self.hidePanel(self.uploadPanel, self.actionTip);
                self.uploadFiles(e.dataTransfer.files);
            });

        }
    })

}

function loadUploader(id, op) {

    let option = op;

    let tagId = id;

    if (!option) return;

    let templateId = "#" + option.templateId;

    if ($(templateId).length) return initialUploader(tagId, option);

    let vueComponent = null;

    let templateContainer = $(document.createElement("script"));

    templateContainer.attr("type", "text/x-template").attr("id", templateId).appendTo("body");

    templateContainer.load(option.templateUrl, function () {
        vueComponent = initialUploader(tagId, option);
    });

    return vueComponent;
}

