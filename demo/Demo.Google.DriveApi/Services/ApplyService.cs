﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Demo.FileHelper;
using Demo.Google.DriveApi.Entities;
using Demo.Google.DriveApi.Models;
using Demo.Google.DriveApi.Stores;
using static Demo.Google.DriveApi.Services.ExplorerService;

namespace Demo.Google.DriveApi.Services
{
    public class ApplyService :IDisposable
    {
        public ApplyService() {
            this.Store = new ApplyStore();
        }
        private ApplyStore Store { get; set; }

        public ServiceInfo GetServiceInfo(int ServiceId)
        {
            var ServiceInfos = Store.GetServices(new int[] { ServiceId });
            if (ServiceInfos.Count() == 0) throw new Exception("not found service");
            return ServiceInfos.FirstOrDefault();
        }

        public ServiceInfo GetServiceInfoByCode(string ServiceCode)
        {
            var ServiceInfos = Store.GetServices(ServiceCodes: new string[] { ServiceCode });
            if (ServiceInfos.Count() == 0) throw new Exception("not found service");
            return ServiceInfos.FirstOrDefault();
        }

        public void CreateService(ServiceInfo ServiceInfo) {
            try
            {                
                if (string.IsNullOrEmpty(ServiceInfo.Code)) throw new Exception("Need Code Field!");
                
                using (var Service = new ExplorerService(ProxyTypes.ServiceAccountAtGoogleDriveProxy))
                {
                    Service.ChangeToRoot();
                    var Directory =  Service.CreateDirectory(ServiceInfo.Code).FirstOrDefault();
                    if (Directory == null) throw new Exception("Can't Not Create Folder For Code");
                    ServiceInfo.Path = Directory.Id;
                }
                Store.CreateService(ServiceInfo);
            }
            catch (Exception ex)
            {
                throw;
            }
        }


        public void DeleteService(string ServiceCode)
        {
            try
            {
                Store.DeleteService(ServiceCode);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public void StopService(string ServiceCode)
        {
            try
            {
                var Service = GetServiceInfoByCode(ServiceCode);
                if (Service == null) throw new Exception("Not Found Service");
                Store.UpdateService(Service);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public IEnumerable<ServiceInfo> ListServices()
        {
            try
            {
                return Store.GetServices();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void Dispose()
        {
            Store.Dispose();
        }

        internal void CheckFiles(string ServCode, IEnumerable<CreateFile> Files)
        {
            try
            {
                var Option = Store.GetServices(ServiceCodes: new List<string>() { ServCode }).FirstOrDefault();
                if (Option == null) throw new Exception("服務代號錯誤!");
                foreach (var File in Files)
                {
                    if (File.Content.Length > Option.Size) throw new Exception("檔案超過大小");
                    if (!Option.Exts.Contains(File.Extension.Replace(".", string.Empty))) throw new Exception("不允許此類型檔案");
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        internal void LogFiles(string ServCode , IEnumerable<CreateFile> Files)
        {
            try
            {
                 Store.LogFiles(ServCode,Files);
            }
            catch (Exception ex)
            {

            }
        }
    }
}