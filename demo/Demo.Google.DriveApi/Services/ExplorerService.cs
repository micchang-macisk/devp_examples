﻿using Demo.FileHelper;
using Demo.FileHelper.Proxy;
using Demo.Google.DriveApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Demo.Google.DriveApi.Services
{
    public class ExplorerService :IDisposable
    {
        FileProxy Proxy { get; set; }
        FileExplorer Explorer { get; set; }
        ServiceInfo ServiceInfo { get; set; }

        #region CredentialOption
        public static Dictionary<ProxyTypes, ICredentialOption> CredentialOptions { get; set; }

        public static void SetCredentialOptions(Dictionary<ProxyTypes, ICredentialOption> Options) {
            CredentialOptions = Options;
        }

        public ICredentialOption CredentialOption { get; set; }

        private ICredentialOption GetCredentialOptionByServiceInfo(ServiceInfo ServiceInfo)
        {
            var HasOption = CredentialOptions.ContainsKey(ServiceInfo.ProxyType);
            if (!HasOption) throw new Exception("none assign option for proxy type");
            return CredentialOptions[ServiceInfo.ProxyType];
        }

        public void ChangeToRoot()
        {
            Explorer.ChangeToDirectory(CredentialOption.Root);
        }
        #endregion


        ApplyService ApplyService { get; set; }

        public enum ProxyTypes {
            ServiceAccountAtGoogleDriveProxy = 10 ,
        }        

        public const string GoogleDriveServiceAccountProxyType = "ServiceAccount@GoogleDriveProxy";

        public ExplorerService(string ServiceCode)
        {
            this.ApplyService = new ApplyService();
            this.ServiceInfo = GetServiceInfoByServiceCode(ServiceCode);
            this.CredentialOption = GetCredentialOptionByServiceInfo(this.ServiceInfo);
            this.Proxy = InitialProxyByServiceInfo(this.ServiceInfo,this.CredentialOption);
            this.Explorer = this.Proxy.GetExplorer();
            SetDirectoryByServiceInfo(this.ServiceInfo,this.Explorer);
        }

        public ExplorerService(ProxyTypes ProxyType)
        {
            this.CredentialOption = CredentialOptions[ProxyType];
            this.Proxy = new GoogleDriveProxy(GoogleDriveProxy.CredentialType.ServiceAccount, this.CredentialOption);
            this.Explorer = this.Proxy.GetExplorer();
        }


        #region Initial 

        private FileProxy InitialProxyByServiceInfo(ServiceInfo ServiceInfo,ICredentialOption Option)
        {
            var Proxy = ServiceInfo.Proxy.ToLower();
            switch (ServiceInfo.ProxyType)
            {
                case ProxyTypes.ServiceAccountAtGoogleDriveProxy:
                    return new GoogleDriveProxy(GoogleDriveProxy.CredentialType.ServiceAccount, Option);
                    break;
                default:
                    return null;
                    break;
            }
        }


        private IFile SetDirectoryByServiceInfo(ServiceInfo Service, FileExplorer Explorer)
        {
            var Path = Explorer.Get(Service.Path).FirstOrDefault();
            if (Path == null) throw new Exception("Not Found Path");
            return SetDirectoryByServiceInfo( Path.Id ,Explorer );
        }

        private IFile SetDirectoryByServiceInfo(string Path ,FileExplorer Explorer)
        {
            return Explorer.ChangeToDirectory(Path).FirstOrDefault();
        }

        private IFileProxy InitialProxyByService(ServiceInfo Service)
        {
            throw new NotImplementedException();
        }

        public ServiceInfo GetServiceInfoByServiceId(int ServiceId)
        {
            return ApplyService.GetServiceInfo(ServiceId);
        }

        public ServiceInfo GetServiceInfoByServiceCode(string ServiceCode)
        {
            return ApplyService.GetServiceInfoByCode(ServiceCode);
        }

    

        #endregion

        public IEnumerable<CreateDirectory> CreateDirectory(string FolderName)
        {
            
            return Explorer.Folder(FolderName);
        }

        public IEnumerable<OnlineFile> ListFiles()
        {
            var Result = Explorer.List(new Condition { DirectoryId = ServiceInfo.Path });
            return Result;
        }


        public IEnumerable<CreateFile> CreateFiles(IEnumerable<CreateFile> Files)
        {
            var rand = new Random();
            foreach (var File in Files)
            {
                File.DisplayName = File.Name;
                var FileCode = DateTime.Now.ToString("yyyyMMddHHmmss_") + rand.Next(9999).ToString("0000");
                var Ext = new System.IO.FileInfo(File.Name).Extension;
                File.Name = $"{FileCode}{Ext}";
            }
            return Explorer.Create(Files);
        }

        public ContentFile GetFileContent(string FileId)
        {
            var Result = Explorer.Content(FileId).FirstOrDefault();
            if (Result == null) throw new Exception("Not Found Content");
            return Result;
        }

        public void DeleteFile(string FileId)
        {
            Explorer.Delete(FileId);
        }
        public void Dispose()
        {

        }
    }
}