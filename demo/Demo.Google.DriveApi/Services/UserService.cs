﻿using Demo.Google.DriveApi.Models;
using Demo.Google.DriveApi.Stores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Demo.Google.DriveApi.Services
{
    public class UserService
    {
        public string SaveUser(User User)
        {
            var Key = User.AccessToken;
            new UserStore().SaveUser(Key,User); 
            return Key;
        }

        public User GetUser(string Key)
        {
            return new UserStore().GetUser(Key);
        }

        public User GetUserByEmail(string Email) {

            return new UserStore().GetUserByEmail(Email);
        }
    }
}