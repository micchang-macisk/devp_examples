﻿using Demo.Google.DriveApi.Models;
using Demo.Google.DriveApi.Services;
using Microsoft.Owin;
using Microsoft.Owin.Security.Google;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Demo.Google.DriveApi
{

    public partial class Startup
    {
        public const string CallBackEndpoint = "/oauth/drive/callback";
        public const string GoogleAuthenticationType = "Google";
        public void Configuration(IAppBuilder App)
        {
            
            App.UseCookieAuthentication(new Microsoft.Owin.Security.Cookies.CookieAuthenticationOptions(){AuthenticationType = "External"});

            //Google外部登入使用者
            var google = new GoogleOAuth2AuthenticationOptions
            {
                AuthenticationType = GoogleAuthenticationType,
                ClientId = Constants.ClientId,
                ClientSecret = Constants.ClientSecret,
                CallbackPath = new PathString(CallBackEndpoint),
                Provider = new GoogleOauth2Provider(),
                SignInAsAuthenticationType = "GoogleSign",
            };

            google.AccessType = "offline";

            google.Scope.Add("openid");
            google.Scope.Add("profile");
            google.Scope.Add("email");
            google.Scope.Add("https://www.googleapis.com/auth/drive.file");
            google.Scope.Add("https://www.googleapis.com/auth/drive.appdata");

            App.UseGoogleAuthentication(google);


        }

    }

    public class GoogleOauth2Provider : GoogleOAuth2AuthenticationProvider
    {

        public override Task ReturnEndpoint(GoogleOAuth2ReturnEndpointContext context)
        {
            return base.ReturnEndpoint(context);
        }
        public override void ApplyRedirect(GoogleOAuth2ApplyRedirectContext context)
        {
            var RedirectUri = context.RedirectUri + "&approval_prompt=force";
            context.Response.Redirect(RedirectUri);      

        }
        public override Task Authenticated(GoogleOAuth2AuthenticatedContext context)
        {
            var Identify = new User() { Email = context.Email , Id = context.Id , AccessToken =context.AccessToken,RefreshToken=context.RefreshToken };
            var Credential = new UserService().SaveUser(Identify);
            HttpContext.Current.Response.Cookies.Add(new HttpCookie("authencation", Credential));
            return base.Authenticated(context);
        }
    }

}