﻿using Demo.FileHelper.Extensions;
using Demo.Google.DriveApi.Entities;
using Demo.Google.DriveApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Demo.Google.DriveApi.Stores
{
    public class ApplyStore:IDisposable
    {

        FileApiDBEntities Db { get; set; }

        public ApplyStore()
        {
            this.Db = new FileApiDBEntities();
        }


        public void LogFiles(string ServCode, IEnumerable<FileHelper.CreateFile> Files) {
            try
            {
                foreach (var File in Files)
                {
                    var Ext = new System.IO.FileInfo(File.Name).Extension;
                    var Display = File.DisplayName.Replace(Ext, string.Empty);
                    var Name = File.Name.Replace(Ext, string.Empty);
                    this.Db.FileLogs.Add(new FileLogs() { File_Id = File.Id , File_Name = Display, File_OrgName = File.Name , Serv_Code = ServCode, File_CreateDate = DateTime.Now , File_Mime = File.MimeType , File_Ext = Ext });
                }
                this.Db.SaveChanges();
            }
            catch (Exception ex)
            {
                return;
            }
        }

        public IEnumerable<ServiceInfo> GetServices(IEnumerable<int> ServiceIds=null,IEnumerable<string> ServiceCodes=null)
        {
            //return new List<ServiceInfo>() { new ServiceInfo() { Id=0, Code="Devp", Memo="Devp", Path= "18djn2rdCyOdqcinhmB7vj_FHPsZwOIyV", Proxy="GoogleDriveProxy.ServiceAccount", Owner="micchang@macisk.idv.tw" } };
            var Query = Db.FileService.AsQueryable();
            
            if (ServiceIds.IsAny<int>())
                Query = Query.Where(c => ServiceIds.Contains(c.Serv_Id));

            if (ServiceCodes.IsAny<string>())
                Query = Query.Where(c => ServiceCodes.Contains(c.Serv_Code));

            var Result = Query.Select(s => new ServiceInfo() {
                  Id = s.Serv_Id, 
                 Code = s.Serv_Code , 
                 Proxy = s.Serv_Proxy , 
                 Path = s.Serv_Path , 
                 Memo = s.Serv_Memo , 
                 Size = s.Serv_Size , 
                 Exts = s.Serv_Exts ,
                 Owner = s.Serv_Owner , 
                 CreateDate = s.Serv_CreateDate , 
                 UpdateDate = s.Serv_UpdateDate
            });

            return Result.ToList();            
        }

        public void CreateService(ServiceInfo ServiceInfo)
        {
            try
            {
                var DbService = new FileService()
                {
                    Serv_Id = ServiceInfo.Id,
                    Serv_Code = ServiceInfo.Code,
                    Serv_Proxy = ServiceInfo.Proxy,
                    Serv_Memo = ServiceInfo.Memo,
                    Serv_Path = ServiceInfo.Path,
                    Serv_Owner = "micchang@macisk.idv.tw",
                    Serv_CreateDate = DateTime.Now,
                    Serv_UpdateDate = DateTime.Now
                }; 
                Db.FileService.Add(DbService);
                Db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void UpdateService(ServiceInfo ServiceInfo)
        {
            try
            {
                var DbService = Db.FileService.FirstOrDefault(c => c.Serv_Id == ServiceInfo.Id);
                if (DbService == null) throw new Exception("Not Found Service");
                DbService.Serv_Path = ServiceInfo.Path;
                DbService.Serv_Proxy = ServiceInfo.Proxy;
                DbService.Serv_Owner = ServiceInfo.Owner;
                DbService.Serv_UpdateDate = DateTime.Now;
                Db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public void DeleteService(string ServiceCode)
        {
            try
            {
                var DbService = Db.FileService.FirstOrDefault(c => c.Serv_Code == ServiceCode);
                if (DbService == null) throw new Exception("Not Found Service");
                Db.FileService.Remove(DbService);
                Db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void Dispose()
        {
            Db.Dispose();
        }
    }
}