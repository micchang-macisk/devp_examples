﻿using Demo.Google.DriveApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Demo.Google.DriveApi.Stores
{
    public class UserStore
    {
        public UserStore() {
            if (UserStore.Users == null) UserStore.Users = new Dictionary<string, User>();
        }
        private static Dictionary<string, User> Users { get; set; }

        public void SaveUser(string Key,User User) {
            if (Users.ContainsKey(Key))
                Users[Key] = User;
            else
                Users.Add(Key, User);
        }
        public User GetUser(string Key)
        {
            if (!Users.ContainsKey(Key)) return null;
            return Users[Key];
        }

        internal User GetUserByEmail(string email)
        {
            var User = Users.FirstOrDefault(c => c.Value.Email == email);
            if (User.Value == null) return null;
            return User.Value;
        }
    }
}