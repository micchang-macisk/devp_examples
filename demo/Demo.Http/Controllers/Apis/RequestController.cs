﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;

namespace Demo.Http.Controllers.Apis
{
    public class RequestController : ApiController
    {
        /// <summary>
        /// 取得通訊資料
        /// </summary>
        /// <returns></returns>
        [Route("api/Request"),HttpGet,HttpPost]
        public IHttpActionResult GetStudInfo(StudInfo StudInfo) {
            return Ok(StudInfo);
        }

        /// <summary>
        /// 取得標頭
        /// </summary>
        /// <returns></returns>
        [Route("api/Request/Files"), HttpGet, HttpPost]
        public async Task<IHttpActionResult> GetFiles()
        {
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            var provider = new MultipartBaseFileProvider();

            try
            {
                await Request.Content.ReadAsMultipartAsync(provider);
                
                foreach (var Content in provider.DataContent)
                {
                    if (Content is BaseFile)
                    {
                        var File = (BaseFile)Content;
                        File.FieldContent = Convert.ToBase64String(File.Stream.ToArray());
                    }
                    else {
                        Content.FieldContent = new StreamReader(Content.Stream).ReadToEnd();
                    }

                }
                return Ok(provider.DataContent);
            }
            catch (System.Exception e)
            {
                return InternalServerError();
            }
        }

        public class BaseContent
        {
            public string FieldName { get; set; }
            public string FieldContent { get; set; }

            [JsonIgnore]
            public MemoryStream Stream { get; set; }
        }

        public class BaseFile : BaseContent
        {
            public string FileName { get; set; }
            
            public string MimeType { get; set; }
        }

        public class StudInfo {
            public string studId { get; set; }
            public string studName { get; set; }
        }

        public class MultipartBaseFileProvider : MultipartStreamProvider
        {
            public MultipartBaseFileProvider() { DataContent = new List<BaseContent>(); }
            public List<BaseContent> DataContent { get; set; }
            public override Stream GetStream(HttpContent parent, HttpContentHeaders Headers)
            {
                BaseContent Content = null;
                if (Headers.ContentType != null)
                {
                    Content = new BaseFile()
                    {
                        FileName = ParseFileName(Headers),
                        MimeType = Headers.ContentType.MediaType,
                        Stream = new MemoryStream(),
                    };
                }
                else {
                    Content = new BaseContent()
                    {
                        FieldContent = string.Empty,
                        Stream = new MemoryStream(),
                    };
                };
                Content.FieldName = Headers.ContentDisposition.Name;
                DataContent.Add(Content);
                return Content.Stream;
            }

            public static string ParseFileName(HttpContentHeaders Headers)
            {
                string FileName = null;
                var ContentDisposition = Headers.ContentDisposition;
                if (ContentDisposition == null)
                {
                    FileName = null;
                    return null;
                }

                FileName = UnquoteToken(ContentDisposition.FileName);
                return FileName;
            }

            public static string UnquoteToken(string Token)
            {
                if (string.IsNullOrWhiteSpace(Token))
                    return Token;

                if (Token.StartsWith("\"", StringComparison.Ordinal) && Token.EndsWith("\"", StringComparison.Ordinal) && Token.Length > 1)
                    return Token.Substring(1, Token.Length - 2);

                return Token;
            }
        }
        
    }
}
