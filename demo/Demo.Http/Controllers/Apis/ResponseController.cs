﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Xml;

namespace Demo.Http.Controllers.Apis
{
    public class ResponseController : ApiController
    {

        [Route("api/response/content_type")]
        public HttpResponseMessage GetXhtml(string type)
        {
            //build a string
            var Xml = GetXml("Xhtml.xml");
            var Result = Request.CreateResponse(HttpStatusCode.OK);
            var contentType = "application/xml";
            var contentdisposition = default(string);
            if (type == "xml")
                contentType = "application/xml";
            if (type == "html")
                contentType = "text/html";
            if (type == "xls")
            {
                contentType = "application/vnd.ms-excel";
                contentdisposition = "attachment;filename=xhtml.xls";
            }
            Result.Content = new StringContent(Xml.OuterXml, Encoding.UTF8, contentType);

            if (contentdisposition!=default(string))
                Result.Content.Headers.Add("content-disposition", contentdisposition);

            return Result;
        }


        [Route("api/response/refresh")]
        public HttpResponseMessage GetRefresh(bool isRedirect)
        {
            //build a string
            var Xml = GetXml("refresh.xml");
            var Result = Request.CreateResponse(HttpStatusCode.OK);
            Result.Content = new StringContent(Xml.OuterXml, Encoding.UTF8, "text/html");
            var refreshContent = "5";
            if (isRedirect)
                refreshContent += ";url=http://www.google.com.tw/"; 
            Result.Content.Headers.Add("refresh", refreshContent);
            return Result;
        }
        
        private XmlDocument GetXml(string FileName) {
            var Xml = new XmlDocument();
            Xml.Load(HttpContext.Current.Server.MapPath("~/App_Data/"+FileName));
            return Xml;
        }

    }
}
