﻿using Demo.Http.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace Demo.Http.Controllers.Apis
{
    public class StatusCodeController : ApiController
    {
        /// <summary>
        /// 取得(200)成功狀態
        /// </summary>
        /// <returns></returns>
        [Route("api/StatusCode/Success"), HttpGet]
        public IHttpActionResult GetSuccess()
        {
            return Ok();
        }


        /// <summary>
        /// 取得301永久移除狀態
        /// </summary>
        /// <returns></returns>
        [Route("api/StatusCode/Moved"), HttpGet]
        public IHttpActionResult GetMoved()
        {
            var Result = new StatusHeaderActionResult(
                HttpStatusCode.Moved
            );
            return Result;
        }


        /// <summary>
        /// 取得301永久導向狀態
        /// </summary>
        /// <returns></returns>
        [Route("api/StatusCode/MovedPermanently"), HttpGet]
        public IHttpActionResult GetMovedPermanently()
        {
            var Result = new StatusHeaderActionResult(
                HttpStatusCode.MovedPermanently , 
                new Dictionary<string, string>() {
                    { "Location" , "/api/StatusCode/BeenNew" }
                }
            );            
            return Result;
        }

        /// <summary>
        /// 取得302臨時導向狀態
        /// </summary>
        /// <returns></returns>
        [Route("api/StatusCode/Found"), HttpGet]
        public IHttpActionResult GetFound()
        {
            return Redirect("http://localhost:49976/api/StatusCode/BeenNew");
        }


        /// <summary>
        /// 取得401授權錯誤
        /// </summary>
        /// <returns></returns>
        [Route("api/StatusCode/Unauthorized"), HttpGet]
        public IHttpActionResult GetUnauthorized()
        {
            return StatusCode(HttpStatusCode.Unauthorized);
        }

        /// <summary>
        /// 取得403身分驗證錯誤
        /// </summary>
        /// <returns></returns>
        [Route("api/StatusCode/Forbidden"), HttpGet]
        public IHttpActionResult GetForbidden()
        {
            return StatusCode(HttpStatusCode.Forbidden); 
        }

        /// <summary>
        /// 取得404身分驗證錯誤
        /// </summary>
        /// <returns></returns>
        [Route("api/StatusCode/NotFound"), HttpGet]
        public IHttpActionResult GetNotFound()
        {
            return StatusCode(HttpStatusCode.NotFound);
        }


        /// <summary>
        /// 已重新導向新頁面
        /// </summary>
        /// <returns></returns>
        [Route("api/StatusCode/BeenNew"), HttpGet]
        public IHttpActionResult GetBeenNew()
        {
            return Ok();
        }

    }
}
