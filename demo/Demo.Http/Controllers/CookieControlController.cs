﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Demo.Http.Controllers
{
    public class CookieControlController : Controller
    {
        // GET: CookieControl
        public ActionResult Start(string Action=null , string CookieName = null ,  string CookieValue = null , string RemoteMode = null, string GetCookieName = null)
        {
            if (string.IsNullOrEmpty(Action)) return View();
            if (Action == "SetRemoteCookie")
            {
                var Cookie = new HttpCookie(CookieName, CookieValue);
                if (RemoteMode == "HttpOnly")
                    Cookie.HttpOnly = true;
                Cookie.Expires = DateTime.Now.AddDays(1);
                Response.AppendCookie(Cookie);
            }
            if (Action == "RemoveRemoteCookie")
            {
                var Cookie = new HttpCookie(GetCookieName);
                Cookie.Expires = DateTime.Now.AddDays(-3);
                Response.Cookies.Add(Cookie);
            }
            return View();
        }
    }
}