﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Demo.Http.Handlers
{
    public class CustomHttpHandler : IHttpHandler
    {
        public bool IsReusable => true;

        public void ProcessRequest(HttpContext context)
        {
            context.Response.Write("Handle Extension for Custom !!");
            context.Response.End();
        }

    }
}