﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Demo.Http.Helpers
{

    public class StatusHeaderActionResult : IHttpActionResult
    {

        HttpStatusCode StatusCode { get; set; }

        Dictionary<string, string> Headers { get; set; }
        object Payload { get; set; }


        public StatusHeaderActionResult(HttpStatusCode StatusCode, Dictionary<string, string> Headers = null,object Payload = null)
        {
            this.StatusCode = StatusCode;

            this.Headers = new Dictionary<string, string>();
            if (Headers != null)
                this.Headers = Headers;

            this.Payload = Payload;

        }

        public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            Task<HttpResponseMessage> ResultTask = new Task<HttpResponseMessage>((objArgs) => {
                Arg Args = (Arg)objArgs;
                HttpResponseMessage Message = new HttpResponseMessage();
                Message.StatusCode = Args.StatusCode;
                foreach (var Header in Args.Headers)
                {
                    Message.Headers.Add(Header.Key, Header.Value);
                }
                return Message;
            }, new Arg { StatusCode = this.StatusCode, Headers = this.Headers });
            ResultTask.Start();
            return ResultTask;
        }

        public class Arg
        {

            public HttpStatusCode StatusCode { get; set; }

            public Dictionary<string, string> Headers { get; set; }

            public object PayLoad { get; set; }
        }

    }
}