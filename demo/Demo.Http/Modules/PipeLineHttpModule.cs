﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Demo.Http.Modules
{
    public class PipeLineHttpModule : IHttpModule
    {
        public void Dispose()
        {
        }

        public void Init(HttpApplication context)
        {
            context.BeginRequest += Context_BeginRequest;
            context.AuthenticateRequest += Context_AuthenticateRequest;
            context.AuthorizeRequest += Context_AuthorizeRequest;
            context.ResolveRequestCache += Context_ResolveRequestCache;
            context.MapRequestHandler += Context_MapRequestHandler;
            context.AcquireRequestState += Context_AcquireRequestState;
            context.PreRequestHandlerExecute += Context_PreRequestHandlerExecute;
            context.ReleaseRequestState += Context_ReleaseRequestState;
            context.UpdateRequestCache += Context_UpdateRequestCache;
            context.LogRequest += Context_LogRequest;
            context.EndRequest += Context_EndRequest;

        }


        private void Context_BeginRequest(object sender, EventArgs e)
        {
            Console.WriteLine("BeginRequest");
        }

        private void Context_AuthenticateRequest(object sender, EventArgs e)
        {
            Console.WriteLine("AuthenticateRequest");
        }
        private void Context_AuthorizeRequest(object sender, EventArgs e)
        {
            Console.WriteLine("AuthorizeRequest");
        }

        private void Context_ResolveRequestCache(object sender, EventArgs e)
        {
            Console.WriteLine("ResolveRequestCache");
        }

        private void Context_MapRequestHandler(object sender, EventArgs e)
        {
            Console.WriteLine("MapRequestHandler");
        }

        private void Context_AcquireRequestState(object sender, EventArgs e)
        {
            Console.WriteLine("AcquireRequestState");
        }
        private void Context_PreRequestHandlerExecute(object sender, EventArgs e)
        {
            Console.WriteLine("PreRequestHandlerExecute");
        }

        private void Context_ReleaseRequestState(object sender, EventArgs e)
        {
            Console.WriteLine("ReleaseRequestState");
        }

        private void Context_UpdateRequestCache(object sender, EventArgs e)
        {
            Console.WriteLine("UpdateRequestCache");
        }

        private void Context_LogRequest(object sender, EventArgs e)
        {
            Console.WriteLine("LogRequest");
        }

        private void Context_EndRequest(object sender, EventArgs e)
        {
            Console.WriteLine("EndRequest");
        }

    }
}