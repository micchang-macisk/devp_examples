﻿
import Vue from 'vue'
import myoffice from 'clientapp/app'


const EventBus = new Vue()

Object.defineProperties(Vue.prototype, {
    $bus: {
        get: function () {
            return EventBus
        }
    }
})
$(function () {

    new Vue(myoffice)
})
