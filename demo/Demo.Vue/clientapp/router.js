﻿import Vue from 'vue'
import router from 'vue-router'
import app from 'clientapp/pages/default'

Vue.use(router)

export default new router({
    routes: [
        {
            path: '/',
            name: 'default',
            component: app
        },
        { path: '*', redirect: '/default' } ,
    ]
})
