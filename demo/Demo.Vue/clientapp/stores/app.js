﻿import setting from 'root/properties/settings.json'
import login from './modules/login.js'
import axios from 'axios'

export const getters = {
    getUserId: userId => state.userId,

}

export const state = {
    ...setting,
    token: "",
    refresh: "",
    header: {
        headers: {
        }
    },
    userId: "",

}

export const actions = {

}

export const mutations = {

}

export default {
    getters,
    state,
    actions,
    mutations,
    modules: { login }
}

