﻿
import Vue from 'vue'
import Vuetify from 'vuetify'
import app from 'clientapp/app'


Vue.use(Vuetify)

import 'vuetify/src/stylus/main'
import 'vuetify/dist/vuetify.min.css'


const EventBus = new Vue()

Object.defineProperties(Vue.prototype, {
    $bus: {
        get: function () {
            return EventBus
        }
    }
})
$(function () {
    new Vue(app)
})
