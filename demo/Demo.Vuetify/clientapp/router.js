﻿import Vue from 'vue'
import router from 'vue-router'
import dashboard from 'clientapp/pages/dashboard'
import portal from 'clientapp/pages/portal'

Vue.use(router)

export default new router({
    routes: [
        {
            path: '/portal/',
            name: 'default',
            component: portal,
            children: [{
                path: 'dashboard',
                component: dashboard
            },]
        },
        { path: '*', redirect: '/portal/dashboard' } ,
    ]
})
