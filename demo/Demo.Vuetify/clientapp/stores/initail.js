﻿import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersistence from 'vuex-persist'

import appStore from './app.js'

Vue.use(Vuex)

export default new Vuex.Store(appStore)

