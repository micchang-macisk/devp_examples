﻿

export const getters = {
    getLoginState: state => state.loginState ,
}


export const state = {
    loginState: false
}

export const actions = {
    signIn({ commit }) {
        commit('setLoginState',true)
    },

}

export const mutations = {
    setLoginState(state, isLogin) {
        state.loginState = isLogin
    },
}

export default {
    getters,
    state,
    actions,
    mutations,
}

