﻿using Demo.WebApi.Handlers;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace Demo.WebApi
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.EnableCors();

            #region 啟用跨域存取
            #endregion

            #region CustomDelegatingHandler 註冊

            ////於 WebApiConfig 中 Route設定 指定特定 CustomDelegatingHandler
            //config.Routes.MapHttpRoute(
            //    name: "CustomApi",
            //    routeTemplate: "custom/{controller}/{id}",
            //    defaults: new { id = RouteParameter.Optional },
            //    constraints: null,
            //    handler: new CustomDelegatingHandler()
            //);

            ////於 WebApiConfig 中 
            ////HttpConfiguration.MessageHandlers 註冊 CustomDelegatingHandler 
            ////所有 Request 都將會被此 CustomDelegatingHandler 檢核識別
            //config.MessageHandlers.Add(new CustomDelegatingHandler());

            #endregion

            #region TokenAuthenticationFilter 註冊
            //於 WebApiConfig 註冊 TokenAuthenticationFilter 
            //所有 Request 都將會被此 TokenAuthenticationFilter 檢核識別
            //config.Filters.Add(new TokenAuthenticationFilter());
            #endregion

            #region IdentifyAuthenticationFilter 註冊
            //於 WebApiConfig 註冊 IdentifyAuthenticationFilter 
            //所有 Request 都將會被此 IdentifyAuthenticationFilter 檢核識別
            //config.Filters.Add(new IdentifyAuthenticationFilter());
            #endregion
            

            #region CustomerAuthorizeFilter 註冊
            //於 WebApiConfig 註冊 CustomerAuthorizeFilter 
            //所有 Request都將會被此 CustomerAuthorizeFilter 檢核權限
            //config.Filters.Add(new CustomerAuthorizeAttribute());
            #endregion

            #region IdentifyActionFilter 註冊
            //於 WebApiConfig 註冊 IdentifyActionFilter 
            //所有 Request都將會被此 IdentifyActionFilter 處理
            //config.Filters.Add(new IdentifyActionFilter());
            #endregion

            #region ExceptionMessageFilter 註冊
            //於 WebApiConfig 註冊 ExceptionMessageFilter 
            //所有的錯誤訊息將都會被此 ExceptionHandler Catch
            //config.Filters.Add(new ExceptionMessageFilterAttribute());
            #endregion

            #region 移除XML輸出格式設定

            //var appXmlType = config.Formatters.XmlFormatter.SupportedMediaTypes.FirstOrDefault(t => t.MediaType == "application/xml");
            //config.Formatters.XmlFormatter.SupportedMediaTypes.Remove(appXmlType);

            #endregion

            #region output case 設定

            //config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            //config.Formatters.JsonFormatter.UseDataContractJsonSerializer = false;

            #endregion

            // Web API 設定和服務

            // Web API 路由

            #region 路由範例

            #region 指定預設值

            //config.Routes.MapHttpRoute(
            //    name: "DefaultApi",
            //    routeTemplate: "api/{controller}/{action}/{id}",
            //    defaults: new { id="10" }
            //);

            #endregion

            #region 指定controller & action 

            //config.Routes.MapHttpRoute(
            //    name: "DefaultApi",
            //    routeTemplate: "api/{controller}/{action}",
            //    defaults: new { }
            //);

            #endregion 

            #endregion

            #region 設定預設路由

            //啟用RouteAttribute
            config.MapHttpAttributeRoutes();
            //預設路由
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            #endregion


        }

    }
}
