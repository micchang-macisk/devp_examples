﻿using Demo.WebApi.Handlers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Demo.WebApi.Controllers.Apis
{

    [IdentifyActionFilter]
    public class ActionFiltersController : ApiController
    {
        public IHttpActionResult Get() {
            return Ok();
        }
    }
}
