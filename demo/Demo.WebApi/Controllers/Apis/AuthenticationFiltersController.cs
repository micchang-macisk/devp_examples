﻿using Demo.WebApi.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Demo.WebApi.Controllers.Apis
{
    public class AuthenticationFiltersController : ApiController
    {
        [SwaggerParameter(Field="Authorization", In= "header",Type ="string",IsRequired=true,Description="帳號")]
        public IHttpActionResult Get() {
            return Ok();
        }
    }
}
