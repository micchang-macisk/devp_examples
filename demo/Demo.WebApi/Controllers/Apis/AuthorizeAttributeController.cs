﻿using Demo.WebApi.Handlers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Demo.WebApi.Controllers.Apis
{
    public class AuthorizeAttributeController : ApiController
    {
        [CustomerAuthorize]
        public IHttpActionResult Get() {
            return Ok();
        }
    }
}
