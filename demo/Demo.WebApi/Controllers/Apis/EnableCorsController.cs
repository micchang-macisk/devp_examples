﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Demo.WebApi.Controllers.Apis
{
    public class EnableCorsController : ApiController
    {
        [EnableCors(
            origins:"*", //開放遠端網域 * 為所有網域
            headers:"*", //開放必須具有特定 Header * 不檢查
            methods:"GET,OPTIONS"   //開放特定動作 * 為所有動作
        )]
        public IHttpActionResult Get() {
            return Ok();
        }

    }
}
