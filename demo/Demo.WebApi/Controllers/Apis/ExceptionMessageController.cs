﻿using Demo.WebApi.Handlers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Demo.WebApi.Controllers.Apis
{
    [ExceptionMessageFilter]
    public class ExceptionMessageController : ApiController
    {
        public IHttpActionResult Post()
        {
            throw new Exception();
            return Ok();
        }

        [ExceptionMessageFilterAttribute]
        public IHttpActionResult Get() {
            throw new Exception();
            return Ok();
        }



    }
}
