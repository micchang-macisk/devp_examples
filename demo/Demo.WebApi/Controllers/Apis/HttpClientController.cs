﻿using Demo.WebApi.Helpers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Demo.WebApi.Controllers.Apis
{
    public class HttpClientController : ApiController
    {
        [Route("httpclient/original")]
        public IHttpActionResult Original()
        {
            using (var Client = new HttpClient())
            {
                //指定讀取的API位置
                var Request = Client.GetAsync("http://localhost:49989/Sort?Skip=2&Take=2&Sort=no");
                //取得Content工作物件
                var Content = Request.Result.Content.ReadAsStringAsync();
                //轉換ContentResult 為 Result Model
                var Result = JsonConvert.DeserializeObject<SortModel>(Content.Result);
                //輸出Result
                return Ok(Result);
            }
        }

        public IHttpActionResult ApiHelper(){
            using (var Client = new HttpClient())
            {
                //透過ApiHelper.ApiGet函式取得結果
                var Result = Client.ApiGet<SortModel>("http://localhost:49989/Sort?Skip=2&Take=2&Sort=no");
                return Ok(Result);
            }
        }
    }

    public class SortModel
    {
        public int no { get; set; }
        public int minus { get; set; }
    }

}
