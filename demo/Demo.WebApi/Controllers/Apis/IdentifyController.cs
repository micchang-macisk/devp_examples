﻿using Demo.WebApi.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Demo.WebApi.Controllers.Apis
{
    
    public class IdentifyController : ApiController
    {
        [SwaggerParameter(Field = "Authorization", In = "header", Type = "string", IsRequired = true, Description = "帳號")]
        [Authorize(Roles ="Member")]
        [Route("identify/center"),HttpGet]
        public IHttpActionResult CenterData() {
            return Ok(Identifies.User);
        }

        [SwaggerParameter(Field = "Authorization", In = "header", Type = "string", IsRequired = true, Description = "帳號")]
        [Authorize(Roles = "Admin")]
        [Route("identify/backend"), HttpGet]
        public IHttpActionResult BackendData()
        {
            return Ok(Identifies.User);
        }

        [SwaggerParameter(Field = "Authorization", In = "header", Type = "string", IsRequired = true, Description = "帳號")]
        [Authorize(Roles = "Guest,Member,Admin")]
        [Route("identify/course"), HttpGet]
        public IHttpActionResult CourseData()
        {
            return Ok(Identifies.User);
        }

    }
}
