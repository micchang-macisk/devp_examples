﻿using Demo.WebApi.Helpers;
using Swashbuckle.Swagger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

using System.Linq.Dynamic;

namespace Demo.WebApi.Controllers.Apis
{
    public class PagingController : ApiController
    {
  
        [Route("Filter")]
        [PagerFilter(SkipField:"from",TakeFiled:"row",SortByFiled:"order")]
        [PagerFilter(SkipField: "skip", TakeFiled: "take", SortByFiled: "sort")]
        public IHttpActionResult PageFilter() {
            return Ok(new { Skip = PageHelper.Skip, Take = PageHelper.Take, SortBy = PageHelper.SortBy });
        }

        [Route("Sort")]
        [PagerFilter]
        public IHttpActionResult SortBy() {
            var Query = Data.AsEnumerable();
            Query = Query.OrderBy(PageHelper.SortBy);
            Query = Query.Skip(PageHelper.Skip).Take(PageHelper.Take);
            return Ok(Query);
        }

        protected class Model {
            public int no { get; set; }
            public int minus { get; set; }
        }
        protected List<Model> Data = new List<Model>() {
            new Model() { no = 1  , minus = 10 } ,
            new Model() { no = 2  , minus = 9 } ,
            new Model() { no = 3  , minus = 8 } ,
            new Model() { no = 4  , minus = 7 } ,
            new Model() { no = 5  , minus = 6 } ,
            new Model() { no = 6  , minus = 5 } ,
            new Model() { no = 7  , minus = 4 } ,
            new Model() { no = 8  , minus = 3 } ,
            new Model() { no = 9  , minus = 2 } ,
            new Model() { no = 10  , minus = 1 } ,
        };
    }
}