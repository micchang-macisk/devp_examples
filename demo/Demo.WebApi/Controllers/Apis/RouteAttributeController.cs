﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Demo.WebApi.Controllers.Apis
{
    public class RouteAttributeController : ApiController
    {
        [Route("api/route/{no:int}/{name}")]
        //[Route("api/routeattr/{no:int}/{name}")]
        [Route("api/routeattribute")]
        public IHttpActionResult Get(int no,string name) {
            return Ok();
        }
    }
}
