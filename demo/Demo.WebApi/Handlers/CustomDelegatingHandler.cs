﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace Demo.WebApi.Handlers
{
    public class CustomDelegatingHandler: DelegatingHandler
    {
        /// <summary>
        /// 可針對 Request 自訂回應內容處理
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        protected override Task<HttpResponseMessage> SendAsync(
            HttpRequestMessage request, CancellationToken cancellationToken)
        {
            #region 附加處理
            //var response = base.SendAsync(request, cancellationToken).Result;
            //response.Headers.Add("Handler", $"({response.Headers.Count(c=>c.Key=="Handler").ToString()}) : Process from DelegatingHandler");
            //return response;
            #endregion

            #region 替代處理
            var response = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent("Hello!")
            };

            var tsc = new TaskCompletionSource<HttpResponseMessage>();
            tsc.SetResult(response); 
            return tsc.Task;
            #endregion
        }
    }

}