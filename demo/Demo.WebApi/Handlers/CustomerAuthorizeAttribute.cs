﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace Demo.WebApi.Handlers
{
    public class CustomerAuthorizeAttribute :AuthorizeAttribute
    {
        protected override bool IsAuthorized(HttpActionContext actionContext)
        #region 可以針對 actionContent.Request讀取 Request 資訊並判斷權限
        {
            return base.IsAuthorized(actionContext);
        }
        #endregion
    }
}

