﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http.Filters;

namespace Demo.WebApi.Handlers
{
    public class ExceptionMessageFilterAttribute : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            #region 可對 HttpActionExecutedContext.Response 加工並輸出需要的訊息及格式
            actionExecutedContext.Response = actionExecutedContext.Request.CreateResponse();
            actionExecutedContext.Response.StatusCode = System.Net.HttpStatusCode.InternalServerError;
            actionExecutedContext.Response.Content = new ExceptionMessage(actionExecutedContext.Exception.Message).GetResultContent();
            #endregion
        }
    }

    public class ExceptionMessage
    {
        public ExceptionMessage(string Message) { this.Message = Message; }
        public string Message { get; set; }

        public HttpContent GetResultContent() {
            var Result = JsonConvert.SerializeObject(this);
            return new StringContent(Result);
        }
    }

}