﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace Demo.WebApi.Handlers
{
    public class IdentifyActionFilter : ActionFilterAttribute
    {        
        public override void OnActionExecuting(HttpActionContext actionContext)
        #region 可針對 actionContext.Request 取得 Reqeust 相關資訊
        {
            base.OnActionExecuting(actionContext);
        }
        #endregion

        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        #region 可針對 actionExecutedContext.Response 自訂回傳格式
        {
            base.OnActionExecuted(actionExecutedContext);
        }
        #endregion        
    }

}