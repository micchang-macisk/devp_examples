﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace Demo.WebApi.Handlers
{
    public class TokenAuthenticationFilter : IAuthenticationFilter
    {
        /// <summary>
        /// 可針對context.Request進行進行辨識處理，並回傳任一該處理動作的Task
        /// </summary>
        /// <param name="context"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task AuthenticateAsync(HttpAuthenticationContext context, CancellationToken cancellationToken)
        {
            AuthenticationHeaderValue token = context.Request.Headers.Authorization;
            if (token == null) return null;
            if (token.Scheme != "Basic ") return null;
            if (string.IsNullOrEmpty(token.Parameter))
            {
                context.ErrorResult = new AuthenticationFailureResult("無識別資料!", context.Request);
                return null;
            }
            if (token.Parameter != "alwaysPass")
            {
                context.ErrorResult = new AuthenticationFailureResult("識別錯誤!", context.Request);
                return null;
            }
            context.Principal = new User("micchang");
            return Task.FromResult(0);
        }
        /// <summary>
        /// 是否可以多線呈使用
        /// </summary>
        public bool AllowMultiple => true;        
        /// <summary>
        /// 若無識別資料，可以針對context.Response 附加資訊給外部
        /// </summary>
        /// <param name="context"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task ChallengeAsync(HttpAuthenticationChallengeContext context, CancellationToken cancellationToken)
        {
            return Task.FromResult(0);
        }        
        private void GetMessageContent(HttpAuthenticationContext context)
        {
            context.ActionContext.Response = context.ActionContext.Request.CreateResponse();
            context.ActionContext.Response.StatusCode = System.Net.HttpStatusCode.Forbidden;
            context.ActionContext.Response.Content =new StringContent(JsonConvert.SerializeObject(new { Message="無法識別來者" }));
        }


        public class User : IPrincipal
        {
            public User(string Name)
            {
                this.Identity = new Identity(Name);
            }

            public IIdentity Identity { get; set; }

            public bool IsInRole(string role)
            {
                return true;
            }
        }

        public class Identity : IIdentity
        {
            public Identity(string Name)
            {
                this.Name = Name;
                this.IsAuthenticated = true;
            }
            public string Name { get; set; }

            public string AuthenticationType => "Basic";

            public bool IsAuthenticated { get; set; }
        }

        public class AuthenticationFailureResult : IHttpActionResult
        {
            public AuthenticationFailureResult(string reasonPhrase, HttpRequestMessage request)
            {
                ReasonPhrase = reasonPhrase;
                Request = request;
            }

            public string ReasonPhrase { get; private set; }

            public HttpRequestMessage Request { get; private set; }

            public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
            {
                return Task.FromResult(Execute());
            }

            private HttpResponseMessage Execute()
            {
                HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.Unauthorized);
                response.RequestMessage = Request;
                response.ReasonPhrase = ReasonPhrase;
                return response;
            }
        }
    }


}