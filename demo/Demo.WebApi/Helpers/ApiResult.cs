﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Demo.WebApi.Helpers
{
    public class ApiResult<TResult>
    {
        public ApiResult(TResult Data, string Message=null)
        {
            this.Data = default(TResult);
            if (Data != null)
                this.Data = Data;
            if (!string.IsNullOrEmpty(Message))
                this.Message = Message;
        }
        public string State { get; set; }
        public string Message { get; set; }
        public TResult Data { get; set; }

    }


}