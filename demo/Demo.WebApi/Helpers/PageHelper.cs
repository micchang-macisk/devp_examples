﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace Demo.WebApi.Helpers
{
    public static class PageHelper
    {
        [ThreadStatic]
        public static int SkipValue = -1;
        public static int Skip
        {
            get { return SkipValue; }
            set { SkipValue = value; }
        }

        [ThreadStatic]
        public static int TakeValue = -1;
        public static int Take
        {
            get { return TakeValue; }
            set { TakeValue = value; }
        }

        [ThreadStatic]
        public static string SortByValue = string.Empty;
        public static string SortBy
        {
            get { return SortByValue; }
            set { SortByValue = value; }
        }
    }

    public class PagerFilterAttribute : ActionFilterAttribute
    {
        public string SkipField { get; set; }
        public string TakeFiled { get; set; }
        public string SortByFiled { get; set; }

        public PagerFilterAttribute(string SkipField = "Skip", string TakeFiled = "Take", string SortByFiled = "Sort")
        {
            if (!string.IsNullOrEmpty(SkipField))
                this.SkipField = SkipField;
            if (!string.IsNullOrEmpty(TakeFiled))
                this.TakeFiled = TakeFiled;
            if (!string.IsNullOrEmpty(SortByFiled))
                this.SortByFiled = SortByFiled;
        }

        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            if(HasProperty(actionContext, SkipField))
                PageHelper.Skip = TryGetIntPropertyValue(actionContext, SkipField);
            if(HasProperty(actionContext, TakeFiled))
                PageHelper.Take = TryGetIntPropertyValue(actionContext, TakeFiled);
            if (HasProperty(actionContext, SortByFiled))
                PageHelper.SortBy = TryGetStringPropertyValue(actionContext, SortByFiled);
        }

        protected bool HasProperty(HttpActionContext Context, string Field)
        {
            var LowerKey = Field.ToLower();
            string Query = Context.Request.RequestUri.Query;
            var NameValues = System.Web.HttpUtility.ParseQueryString(Query);
            return !string.IsNullOrEmpty(NameValues[LowerKey]);
        }

        protected int TryGetIntPropertyValue(HttpActionContext Context, string Field)
        {
            try
            {
                var ValueContext = TryGetStringPropertyValue(Context, Field);
                int Value = -1;
                int.TryParse(ValueContext, out Value);
                return Value;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }

        protected string TryGetStringPropertyValue(HttpActionContext Context, string Field)
        {
            try
            {
                var LowerKey = Field.ToLower();
                string Query = Context.Request.RequestUri.Query;
                var NameValues = System.Web.HttpUtility.ParseQueryString(Query);
                var Property = NameValues[LowerKey];
                var HasProperty = !string.IsNullOrEmpty(Property);
                if (!HasProperty) return string.Empty;
                var ValueContext = Property;
                return ValueContext;
            }
            catch (Exception ex)
            {
                return string.Empty;
            }
        }


    }


    public class ApiPageResult<TResult> : ApiResult<IEnumerable<TResult>>
    {

        public ApiPageResult(IEnumerable<TResult> Data, string Message = null) : base(Data, Message)
        {
            this.Data = new List<TResult>();
            if (Data != null)
                this.Data = Data;
            if (!string.IsNullOrEmpty(Message))
                this.Message = Message;
        }

        public int TotalCount { get; set; }

    }


}