﻿using Swashbuckle.Swagger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Description;
using System.Web.Http.Filters;

namespace Demo.WebApi.Helpers
{
    public enum ParameterIn
    {
        header = 10,
        path = 20,
        query = 30
    }
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    public class SwaggerParameterAttribute : Attribute
    {


        public string Field { get; set; }
        public string In { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
        public string Default { get; set; }
        public bool IsRequired { get; set; }

        public bool AllowMultiple => throw new NotImplementedException();

        public SwaggerParameterAttribute()
        {
        }

        public Task<HttpResponseMessage> ExecuteActionFilterAsync(HttpActionContext actionContext, CancellationToken cancellationToken, Func<Task<HttpResponseMessage>> continuation)
        {
            throw new NotImplementedException();
        }
    }
    public class SwaggerParameterOperationFilter : IOperationFilter
    {
        public void Apply(Operation operation, SchemaRegistry schemaRegistry, ApiDescription apiDescription)
        {
            if (operation.parameters == null)
                operation.parameters = new List<Parameter>();

            var Parameters = apiDescription.ActionDescriptor.GetCustomAttributes<SwaggerParameterAttribute>();
            foreach (var Parameter in Parameters)
            {
                operation.parameters.Add(new Parameter
                {
                    name = Parameter.Field,
                    @in = Parameter.In.ToString(),
                    type = Parameter.Type,
                    required = Parameter.IsRequired,
                    description = Parameter.Description

                });
            }

            var Pagers = apiDescription.ActionDescriptor.GetCustomAttributes<PagerFilterAttribute>();
            foreach (var Pager in Pagers)
            {

                operation.parameters.Add(new Parameter
                {
                    name = Pager.SkipField,
                    @in = ParameterIn.query.ToString(),
                    type = "integer",
                    required = false,
                    description = "忽略筆數"

                });

                operation.parameters.Add(new Parameter
                {
                    name = Pager.TakeFiled,
                    @in = ParameterIn.query.ToString(),
                    type = "integer",
                    required = false,
                    description = "取得筆數"
                });

                operation.parameters.Add(new Parameter
                {
                    name = Pager.SortByFiled,
                    @in = ParameterIn.query.ToString(),
                    type = "string",
                    required = false,
                    description = "資料排序"
                });
            }

        }
    }
}