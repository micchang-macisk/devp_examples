﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static Demo.WebApi.Handlers.IdentifyAuthenticationFilter;

namespace Demo.WebApi
{
    public class Identifies
    {
        public static User User {
            get {
                try
                {
                    return (User)HttpContext.Current.User;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }
    }
}