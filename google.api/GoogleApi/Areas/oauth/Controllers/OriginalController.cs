﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace GoogleApi.Areas.oauth.Controllers
{
    public class OriginalController : Controller
    {
        // GET: oauth/Original
        [Route("oauth/original/start")]
        public ActionResult Start()
        {
            return View();
        }

        [Route("oauth/original/oauth")]
        public ActionResult OAuth(string ClientId, string ResponseType, string Scope, string RedirectUrl, string State)
        {
            var RedUrl = $"https://accounts.google.com/o/oauth2/v2/auth?client_id={ClientId}&response_type={ResponseType}&scope={Server.UrlEncode(Scope)}&redirect_uri={Server.UrlEncode(RedirectUrl)}&state={State}";
            return Redirect(RedUrl);
        }

        [Route("oauth/original/callback")]
        public ActionResult CallBack()
        {
            return View();
        }

        [Route("oauth/original/exchange")]
        public ActionResult Exchange(string State, string Code, string Scope)
        {
            using (var client = new HttpClient())
            {
                var IdCodeDisplay = new IdCodeDisplay();
                var Result = client.PostAsJsonAsync("https://www.googleapis.com/oauth2/v4/token", new
                {
                    code = Code,
                    client_id = Constants.ClientId,
                    client_secret = Constants.ClientSecret,
                    redirect_uri = $"http://localhost:63293{Url.Action("CallBack").ToLower()}",
                    grant_type = "authorization_code",
                    access_type = "offline"
                }).Result.Content.ReadAsStringAsync().Result;
                IdCodeDisplay.IdCode = JsonConvert.DeserializeObject<IdCode>(Result);
                IdCodeDisplay.ResponseContent = JsonConvert.SerializeObject(IdCodeDisplay.IdCode, Formatting.Indented);
                return View(IdCodeDisplay);
            }
        }

        [Route("oauth/original/idtokeninfo")]
        public ActionResult IdTokenInfo(string id_token)
        {
            using (var client = new HttpClient())
            {
                var IdTokenDisplay = new IdTokenDisplay();
                var Result = client.GetStringAsync("https://www.googleapis.com/oauth2/v3/tokeninfo?id_token="+ id_token).Result;
                IdTokenDisplay.TokenInfo = JsonConvert.DeserializeObject<TokenInfo>(Result);
                IdTokenDisplay.ResponseContent = JsonConvert.SerializeObject(IdTokenDisplay.TokenInfo, Formatting.Indented);
                return View(IdTokenDisplay);
            }
        }


        [Route("oauth/original/userinfo")]
        public ActionResult UserInfo()
        {
            return View();
        }

    }


}

public class OAuthUser
{
    public string Id { get; set; }
    public string User { get; set; }
    public string Email { get; set; }

}

public class IdCodeDisplay
{

    public string ResponseContent { get; set; }
    public IdCode IdCode { get; set; }

}

public class IdCode
{
    public string access_token { get; set; }
    public int expires_in { get; set; }
    public string scope { get; set; }
    public string token_type { get; set; }
    public string id_token { get; set; }
}

public class IdTokenDisplay
{

    public string ResponseContent { get; set; }

    public TokenInfo TokenInfo { get; set; }
}

public class TokenInfo
{
    public string iss { get; set; }
    public string azp { get; set; }
    public string aud { get; set; }
    public string sub { get; set; }
    public string hd { get; set; }
    public string email { get; set; }
    public string email_verified { get; set; }
    public string at_hash { get; set; }
    public string name { get; set; }
    public string picture { get; set; }
    public string given_name { get; set; }
    public string family_name { get; set; }
    public string locale { get; set; }
    public string iat { get; set; }
    public string exp { get; set; }
    public string alg { get; set; }
    public string kid { get; set; }
    public string typ { get; set; }
}
