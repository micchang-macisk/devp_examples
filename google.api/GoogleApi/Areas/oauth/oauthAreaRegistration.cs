﻿using System.Web.Mvc;

namespace GoogleApi.Areas.oauth
{
    public class oauthAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "oauth";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "oauth_default",
                "oauth/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}